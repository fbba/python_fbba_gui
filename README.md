# The Fast Beam Based Alignement Project

[![Python 2.7](https://img.shields.io/badge/python-2.7.13-green.svg)](https://www.python.org/downloads/release/python-2713/)



## Introduction

In this repository you can find the compilation of all scripts related to the Fast Beam Based Alignement measurement with a Python GUI.

For more information about the measurement method, please read the paper published on [Physical Review Accelerators and Beams](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.012802)

## Licencing
All python code is GNU-GPLv3 licenced. Author and code mantainer calls for respecting the terms of this license. In case licence therms will be not respected, please contact author.

In case you will use this code, or parts of code contained in this repository, please cite.

## Contact

You can contact pyhton code author at: emorales@cells.es.




