#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    Copyright (C) 2020  Zeus Martí Díaz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""


fh = 6.9464
fv = 5.9537
fskew = 4.0984
fs = 9957.9240636
freqsX = [0, fh/fs, fv/fs]
freqsY = [0, fv/fs, fh/fs]
WvFScaling = 0.4
WvScalingSkew = 2.5
WVFStepSize = 80
wvf_num = 9
wvf_num_skew = 1
QuadDelta = 2.5397
nuseconds = 5


skew_measure_dict = {'sr01/di/bpm-03': ['sr01/di/bpm-acq-03', 'sr16/pc/corh-05', 'sr09/pc/corv-02', 'sr01/pc/qsk-01', 2],
                     'sr01/di/bpm-05': ['sr01/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr13/pc/corv-02', 'sr01/pc/qsk-02', 4],
                     'sr02/di/bpm-03': ['sr02/di/bpm-acq-03', 'sr01/pc/corh-01', 'sr13/pc/corv-02', 'sr02/pc/qsk-01', 9],
                     'sr02/di/bpm-06': ['sr02/di/bpm-acq-06', 'sr01/pc/corh-01', 'sr01/pc/corv-02', 'sr02/pc/qsk-02', 12],
                     'sr03/di/bpm-03': ['sr03/di/bpm-acq-03', 'sr04/pc/corh-05', 'sr01/pc/corv-02', 'sr03/pc/qsk-01', 17],
                     'sr03/di/bpm-06': ['sr03/di/bpm-acq-06', 'sr04/pc/corh-05', 'sr12/pc/corv-04', 'sr03/pc/qsk-02', 20],
                     'sr04/di/bpm-03': ['sr04/di/bpm-acq-03', 'sr05/pc/corh-01', 'sr08/pc/corv-04', 'sr04/pc/qsk-01', 25],
                     'sr04/di/bpm-05': ['sr04/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr12/pc/corv-04', 'sr04/pc/qsk-02', 27],
                     'sr05/di/bpm-03': ['sr05/di/bpm-acq-03', 'sr04/pc/corh-05', 'sr13/pc/corv-02', 'sr05/pc/qsk-01', 32],
                     'sr05/di/bpm-05': ['sr05/di/bpm-acq-05', 'sr04/pc/corh-05', 'sr01/pc/corv-02', 'sr05/pc/qsk-02', 34],
                     'sr06/di/bpm-03': ['sr06/di/bpm-acq-03', 'sr05/pc/corh-01', 'sr01/pc/corv-02', 'sr06/pc/qsk-01', 39],
                     'sr06/di/bpm-06': ['sr06/di/bpm-acq-06', 'sr05/pc/corh-01', 'sr05/pc/corv-02', 'sr06/pc/qsk-02', 42],
                     'sr07/di/bpm-03': ['sr07/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr05/pc/corv-02', 'sr07/pc/qsk-01', 47],
                     'sr07/di/bpm-06': ['sr07/di/bpm-acq-06', 'sr08/pc/corh-05', 'sr12/pc/corv-04', 'sr07/pc/qsk-02', 50],
                     'sr08/di/bpm-03': ['sr08/di/bpm-acq-03', 'sr09/pc/corh-01', 'sr12/pc/corv-04', 'sr08/pc/qsk-01', 55],
                     'sr08/di/bpm-05': ['sr08/di/bpm-acq-05', 'sr09/pc/corh-01', 'sr16/pc/corv-04', 'sr08/pc/qsk-02', 57],
                     'sr09/di/bpm-03': ['sr09/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr01/pc/corv-02', 'sr09/pc/qsk-01', 62],
                     'sr09/di/bpm-05': ['sr09/di/bpm-acq-05', 'sr08/pc/corh-05', 'sr05/pc/corv-02', 'sr09/pc/qsk-02', 64],
                     'sr10/di/bpm-03': ['sr10/di/bpm-acq-03', 'sr09/pc/corh-01', 'sr05/pc/corv-02', 'sr10/pc/qsk-01', 69],
                     'sr10/di/bpm-06': ['sr10/di/bpm-acq-06', 'sr09/pc/corh-01', 'sr09/pc/corv-02', 'sr10/pc/qsk-02', 72],
                     'sr11/di/bpm-03': ['sr11/di/bpm-acq-03', 'sr12/pc/corh-05', 'sr09/pc/corv-02', 'sr11/pc/qsk-01', 77],
                     'sr11/di/bpm-06': ['sr11/di/bpm-acq-06', 'sr12/pc/corh-05', 'sr16/pc/corv-04', 'sr11/pc/qsk-02', 80],
                     'sr12/di/bpm-03': ['sr12/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr16/pc/corv-04', 'sr12/pc/qsk-01', 85],
                     'sr12/di/bpm-05': ['sr12/di/bpm-acq-05', 'sr13/pc/corh-01', 'sr04/pc/corv-04', 'sr12/pc/qsk-02', 87],
                     'sr13/di/bpm-03': ['sr13/di/bpm-acq-03', 'sr12/pc/corh-05', 'sr05/pc/corv-02', 'sr13/pc/qsk-01', 92],
                     'sr13/di/bpm-05': ['sr13/di/bpm-acq-05', 'sr12/pc/corh-05', 'sr09/pc/corv-02', 'sr13/pc/qsk-02', 94],
                     'sr14/di/bpm-03': ['sr14/di/bpm-acq-03', 'sr13/pc/corh-01', 'sr09/pc/corv-02', 'sr14/pc/qsk-01', 99],
                     'sr14/di/bpm-06': ['sr14/di/bpm-acq-06', 'sr13/pc/corh-01', 'sr13/pc/corv-02', 'sr14/pc/qsk-02', 102],
                     'sr15/di/bpm-03': ['sr15/di/bpm-acq-03', 'sr16/pc/corh-05', 'sr16/pc/corv-04', 'sr15/pc/qsk-01', 107],
                     'sr15/di/bpm-06': ['sr15/di/bpm-acq-06', 'sr16/pc/corh-05', 'sr04/pc/corv-04', 'sr15/pc/qsk-02', 110],
                     'sr16/di/bpm-03': ['sr16/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr04/pc/corv-04', 'sr16/pc/qsk-01', 115],
                     'sr16/di/bpm-05': ['sr16/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr08/pc/corv-04', 'sr16/pc/qsk-02', 117]}


quads_measure_dict = {'sr01/di/bpm-01': ['sr01/di/bpm-acq-01', 'sr05/pc/corh-01', 'sr16/pc/corv-04', 'sr01/pc/qv01', 0],
                      'sr01/di/bpm-02': ['sr01/di/bpm-acq-02', 'sr09/pc/corh-01', 'sr09/pc/corv-02', 'sr01/pc/qh02', 1],
                      'sr01/di/bpm-03': ['sr01/di/bpm-acq-03', 'sr16/pc/corh-05', 'sr09/pc/corv-02', 'sr01/pc/qh03', 2],
                      'sr01/di/bpm-04': ['sr01/di/bpm-acq-04', 'sr16/pc/corh-05', 'sr13/pc/corv-02', 'sr01/pc/qh05', 3],
                      'sr01/di/bpm-05': ['sr01/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr13/pc/corv-02', 'sr01/pc/qh05', 4],
                      'sr01/di/bpm-06': ['sr01/di/bpm-acq-06', 'sr08/pc/corh-05', 'sr13/pc/corv-02', 'sr01/pc/qh06', 5],
                      'sr01/di/bpm-07': ['sr01/di/bpm-acq-07', 'sr12/pc/corh-05', 'sr04/pc/corv-04', 'sr01/pc/qv02', 6],
                      'sr02/di/bpm-01': ['sr02/di/bpm-acq-01', 'sr01/pc/corh-01', 'sr09/pc/corv-02', 'sr02/pc/qv03', 7],
                      'sr02/di/bpm-02': ['sr02/di/bpm-acq-02', 'sr03/pc/corh-03', 'sr09/pc/corv-02', 'sr02/pc/qh07', 8],
                      'sr02/di/bpm-03': ['sr02/di/bpm-acq-03', 'sr01/pc/corh-01', 'sr13/pc/corv-02', 'sr02/pc/qh08', 9],
                      'sr02/di/bpm-04': ['sr02/di/bpm-acq-04', 'sr01/pc/corh-01', 'sr13/pc/corv-02', 'sr02/pc/qh08', 10],
                      'sr02/di/bpm-05': ['sr02/di/bpm-acq-05', 'sr01/pc/corh-01', 'sr01/pc/corv-02', 'sr02/pc/qh09', 11],
                      'sr02/di/bpm-06': ['sr02/di/bpm-acq-06', 'sr01/pc/corh-01', 'sr01/pc/corv-02', 'sr02/pc/qh09', 12],
                      'sr02/di/bpm-07': ['sr02/di/bpm-acq-07', 'sr13/pc/corh-01', 'sr01/pc/corv-02', 'sr02/pc/qh10', 13],
                      'sr02/di/bpm-08': ['sr02/di/bpm-acq-08', 'sr01/pc/corh-01', 'sr08/pc/corv-04', 'sr02/pc/qv04', 14],
                      'sr03/di/bpm-01': ['sr03/di/bpm-acq-01', 'sr04/pc/corh-05', 'sr13/pc/corv-02', 'sr03/pc/qv04', 15],
                      'sr03/di/bpm-02': ['sr03/di/bpm-acq-02', 'sr08/pc/corh-05', 'sr04/pc/corv-04', 'sr03/pc/qh10', 16],
                      'sr03/di/bpm-03': ['sr03/di/bpm-acq-03', 'sr04/pc/corh-05', 'sr01/pc/corv-02', 'sr03/pc/qh09', 17],
                      'sr03/di/bpm-04': ['sr03/di/bpm-acq-04', 'sr04/pc/corh-05', 'sr01/pc/corv-02', 'sr03/pc/qh09', 18],
                      'sr03/di/bpm-05': ['sr03/di/bpm-acq-05', 'sr04/pc/corh-05', 'sr08/pc/corv-04', 'sr03/pc/qh08', 19],
                      'sr03/di/bpm-06': ['sr03/di/bpm-acq-06', 'sr04/pc/corh-05', 'sr12/pc/corv-04', 'sr03/pc/qh08', 20],
                      'sr03/di/bpm-07': ['sr03/di/bpm-acq-07', 'sr07/pc/corh-03', 'sr12/pc/corv-04', 'sr03/pc/qh07', 21],
                      'sr03/di/bpm-08': ['sr03/di/bpm-acq-08', 'sr04/pc/corh-05', 'sr12/pc/corv-04', 'sr03/pc/qv03', 22],
                      'sr04/di/bpm-01': ['sr04/di/bpm-acq-01', 'sr09/pc/corh-01', 'sr01/pc/corv-02', 'sr04/pc/qv02', 23],
                      'sr04/di/bpm-02': ['sr04/di/bpm-acq-02', 'sr13/pc/corh-01', 'sr08/pc/corv-04', 'sr04/pc/qh06', 24],
                      'sr04/di/bpm-03': ['sr04/di/bpm-acq-03', 'sr05/pc/corh-01', 'sr08/pc/corv-04', 'sr04/pc/qh05', 25],
                      'sr04/di/bpm-04': ['sr04/di/bpm-acq-04', 'sr05/pc/corh-01', 'sr12/pc/corv-04', 'sr04/pc/qh04', 26],
                      'sr04/di/bpm-05': ['sr04/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr12/pc/corv-04', 'sr04/pc/qh03', 27],
                      'sr04/di/bpm-06': ['sr04/di/bpm-acq-06', 'sr08/pc/corh-05', 'sr12/pc/corv-04', 'sr04/pc/qh02', 28],
                      'sr04/di/bpm-07': ['sr04/di/bpm-acq-07', 'sr12/pc/corh-05', 'sr05/pc/corv-02', 'sr04/pc/qv01', 29],
                      'sr05/di/bpm-01': ['sr05/di/bpm-acq-01', 'sr09/pc/corh-01', 'sr13/pc/corv-02', 'sr05/pc/qv01', 30],
                      'sr05/di/bpm-02': ['sr05/di/bpm-acq-02', 'sr13/pc/corh-01', 'sr13/pc/corv-02', 'sr05/pc/qh02', 31],
                      'sr05/di/bpm-03': ['sr05/di/bpm-acq-03', 'sr04/pc/corh-05', 'sr13/pc/corv-02', 'sr05/pc/qh03', 32],
                      'sr05/di/bpm-04': ['sr05/di/bpm-acq-04', 'sr04/pc/corh-05', 'sr01/pc/corv-02', 'sr05/pc/qh05', 33],
                      'sr05/di/bpm-05': ['sr05/di/bpm-acq-05', 'sr04/pc/corh-05', 'sr01/pc/corv-02', 'sr05/pc/qh05', 34],
                      'sr05/di/bpm-06': ['sr05/di/bpm-acq-06', 'sr08/pc/corh-05', 'sr01/pc/corv-02', 'sr05/pc/qh06', 35],
                      'sr05/di/bpm-07': ['sr05/di/bpm-acq-07', 'sr16/pc/corh-05', 'sr05/pc/corv-02', 'sr05/pc/qv02', 36],
                      'sr06/di/bpm-01': ['sr06/di/bpm-acq-01', 'sr05/pc/corh-01', 'sr13/pc/corv-02', 'sr06/pc/qv03', 37],
                      'sr06/di/bpm-02': ['sr06/di/bpm-acq-02', 'sr05/pc/corh-01', 'sr13/pc/corv-02', 'sr06/pc/qh07', 38],
                      'sr06/di/bpm-03': ['sr06/di/bpm-acq-03', 'sr05/pc/corh-01', 'sr01/pc/corv-02', 'sr06/pc/qh08', 39],
                      'sr06/di/bpm-04': ['sr06/di/bpm-acq-04', 'sr05/pc/corh-01', 'sr01/pc/corv-02', 'sr06/pc/qh08', 40],
                      'sr06/di/bpm-05': ['sr06/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr05/pc/corv-02', 'sr06/pc/qh09', 41],
                      'sr06/di/bpm-06': ['sr06/di/bpm-acq-06', 'sr05/pc/corh-01', 'sr05/pc/corv-02', 'sr06/pc/qh09', 42],
                      'sr06/di/bpm-07': ['sr06/di/bpm-acq-07', 'sr05/pc/corh-01', 'sr05/pc/corv-02', 'sr06/pc/qh10', 43],
                      'sr06/di/bpm-08': ['sr06/di/bpm-acq-08', 'sr05/pc/corh-01', 'sr12/pc/corv-04', 'sr06/pc/qv04', 44],
                      'sr07/di/bpm-01': ['sr07/di/bpm-acq-01', 'sr08/pc/corh-05', 'sr01/pc/corv-02', 'sr07/pc/qv04', 45],
                      'sr07/di/bpm-02': ['sr07/di/bpm-acq-02', 'sr12/pc/corh-05', 'sr05/pc/corv-02', 'sr07/pc/qh10', 46],
                      'sr07/di/bpm-03': ['sr07/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr05/pc/corv-02', 'sr07/pc/qh09', 47],
                      'sr07/di/bpm-04': ['sr07/di/bpm-acq-04', 'sr08/pc/corh-05', 'sr05/pc/corv-02', 'sr07/pc/qh09', 48],
                      'sr07/di/bpm-05': ['sr07/di/bpm-acq-05', 'sr08/pc/corh-05', 'sr12/pc/corv-04', 'sr07/pc/qh08', 49],
                      'sr07/di/bpm-06': ['sr07/di/bpm-acq-06', 'sr08/pc/corh-05', 'sr12/pc/corv-04', 'sr07/pc/qh08', 50],
                      'sr07/di/bpm-07': ['sr07/di/bpm-acq-07', 'sr06/pc/corh-04', 'sr16/pc/corv-04', 'sr07/pc/qh07', 51],
                      'sr07/di/bpm-08': ['sr07/di/bpm-acq-08', 'sr08/pc/corh-05', 'sr16/pc/corv-04', 'sr07/pc/qv03', 52],
                      'sr08/di/bpm-01': ['sr08/di/bpm-acq-01', 'sr13/pc/corh-01', 'sr05/pc/corv-02', 'sr08/pc/qv02', 53],
                      'sr08/di/bpm-02': ['sr08/di/bpm-acq-02', 'sr05/pc/corh-01', 'sr12/pc/corv-04', 'sr08/pc/qh06', 54],
                      'sr08/di/bpm-03': ['sr08/di/bpm-acq-03', 'sr09/pc/corh-01', 'sr12/pc/corv-04', 'sr08/pc/qh05', 55],
                      'sr08/di/bpm-04': ['sr08/di/bpm-acq-04', 'sr09/pc/corh-01', 'sr16/pc/corv-04', 'sr08/pc/qh04', 56],
                      'sr08/di/bpm-05': ['sr08/di/bpm-acq-05', 'sr09/pc/corh-01', 'sr16/pc/corv-04', 'sr08/pc/qh03', 57],
                      'sr08/di/bpm-06': ['sr08/di/bpm-acq-06', 'sr16/pc/corh-05', 'sr16/pc/corv-04', 'sr08/pc/qh02', 58],
                      'sr08/di/bpm-07': ['sr08/di/bpm-acq-07', 'sr04/pc/corh-05', 'sr09/pc/corv-02', 'sr08/pc/qv01', 59],
                      'sr09/di/bpm-01': ['sr09/di/bpm-acq-01', 'sr13/pc/corh-01', 'sr01/pc/corv-02', 'sr09/pc/qv01', 60],
                      'sr09/di/bpm-02': ['sr09/di/bpm-acq-02', 'sr05/pc/corh-01', 'sr01/pc/corv-02', 'sr09/pc/qh02', 61],
                      'sr09/di/bpm-03': ['sr09/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr01/pc/corv-02', 'sr09/pc/qh03', 62],
                      'sr09/di/bpm-04': ['sr09/di/bpm-acq-04', 'sr08/pc/corh-05', 'sr05/pc/corv-02', 'sr09/pc/qh05', 63],
                      'sr09/di/bpm-05': ['sr09/di/bpm-acq-05', 'sr08/pc/corh-05', 'sr05/pc/corv-02', 'sr09/pc/qh05', 64],
                      'sr09/di/bpm-06': ['sr09/di/bpm-acq-06', 'sr16/pc/corh-05', 'sr05/pc/corv-02', 'sr09/pc/qh06', 65],
                      'sr09/di/bpm-07': ['sr09/di/bpm-acq-07', 'sr04/pc/corh-05', 'sr12/pc/corv-04', 'sr09/pc/qv02', 66],
                      'sr10/di/bpm-01': ['sr10/di/bpm-acq-01', 'sr09/pc/corh-01', 'sr01/pc/corv-02', 'sr10/pc/qv03', 67],
                      'sr10/di/bpm-02': ['sr10/di/bpm-acq-02', 'sr09/pc/corh-01', 'sr01/pc/corv-02', 'sr10/pc/qh07', 68],
                      'sr10/di/bpm-03': ['sr10/di/bpm-acq-03', 'sr09/pc/corh-01', 'sr05/pc/corv-02', 'sr10/pc/qh08', 69],
                      'sr10/di/bpm-04': ['sr10/di/bpm-acq-04', 'sr09/pc/corh-01', 'sr05/pc/corv-02', 'sr10/pc/qh08', 70],
                      'sr10/di/bpm-05': ['sr10/di/bpm-acq-05', 'sr09/pc/corh-01', 'sr09/pc/corv-02', 'sr10/pc/qh09', 71],
                      'sr10/di/bpm-06': ['sr10/di/bpm-acq-06', 'sr09/pc/corh-01', 'sr09/pc/corv-02', 'sr10/pc/qh09', 72],
                      'sr10/di/bpm-07': ['sr10/di/bpm-acq-07', 'sr05/pc/corh-01', 'sr09/pc/corv-02', 'sr10/pc/qh10', 73],
                      'sr10/di/bpm-08': ['sr10/di/bpm-acq-08', 'sr09/pc/corh-01', 'sr16/pc/corv-04', 'sr10/pc/qv04', 74],
                      'sr11/di/bpm-01': ['sr11/di/bpm-acq-01', 'sr12/pc/corh-05', 'sr05/pc/corv-02', 'sr11/pc/qv04', 75],
                      'sr11/di/bpm-02': ['sr11/di/bpm-acq-02', 'sr16/pc/corh-05', 'sr12/pc/corv-04', 'sr11/pc/qh10', 76],
                      'sr11/di/bpm-03': ['sr11/di/bpm-acq-03', 'sr12/pc/corh-05', 'sr09/pc/corv-02', 'sr11/pc/qh09', 77],
                      'sr11/di/bpm-04': ['sr11/di/bpm-acq-04', 'sr12/pc/corh-05', 'sr09/pc/corv-02', 'sr11/pc/qh09', 78],
                      'sr11/di/bpm-05': ['sr11/di/bpm-acq-05', 'sr12/pc/corh-05', 'sr16/pc/corv-04', 'sr11/pc/qh08', 79],
                      'sr11/di/bpm-06': ['sr11/di/bpm-acq-06', 'sr12/pc/corh-05', 'sr16/pc/corv-04', 'sr11/pc/qh08', 80],
                      'sr11/di/bpm-07': ['sr11/di/bpm-acq-07', 'sr12/pc/corh-05', 'sr04/pc/corv-04', 'sr11/pc/qh07', 81],
                      'sr11/di/bpm-08': ['sr11/di/bpm-acq-08', 'sr12/pc/corh-05', 'sr13/pc/corv-02', 'sr11/pc/qv03', 82],
                      'sr12/di/bpm-01': ['sr12/di/bpm-acq-01', 'sr05/pc/corh-01', 'sr09/pc/corv-02', 'sr12/pc/qv02', 83],
                      'sr12/di/bpm-02': ['sr12/di/bpm-acq-02', 'sr05/pc/corh-01', 'sr16/pc/corv-04', 'sr12/pc/qh06', 84],
                      'sr12/di/bpm-03': ['sr12/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr16/pc/corv-04', 'sr12/pc/qh05', 85],
                      'sr12/di/bpm-04': ['sr12/di/bpm-acq-04', 'sr13/pc/corh-01', 'sr16/pc/corv-04', 'sr12/pc/qh04', 86],
                      'sr12/di/bpm-05': ['sr12/di/bpm-acq-05', 'sr13/pc/corh-01', 'sr04/pc/corv-04', 'sr12/pc/qh03', 87],
                      'sr12/di/bpm-06': ['sr12/di/bpm-acq-06', 'sr04/pc/corh-05', 'sr04/pc/corv-04', 'sr12/pc/qh02', 88],
                      'sr12/di/bpm-07': ['sr12/di/bpm-acq-07', 'sr08/pc/corh-05', 'sr13/pc/corv-02', 'sr12/pc/qv01', 89],
                      'sr13/di/bpm-01': ['sr13/di/bpm-acq-01', 'sr05/pc/corh-01', 'sr12/pc/corv-04', 'sr13/pc/qv01', 90],
                      'sr13/di/bpm-02': ['sr13/di/bpm-acq-02', 'sr05/pc/corh-01', 'sr05/pc/corv-02', 'sr13/pc/qh02', 91],
                      'sr13/di/bpm-03': ['sr13/di/bpm-acq-03', 'sr12/pc/corh-05', 'sr05/pc/corv-02', 'sr13/pc/qh03', 92],
                      'sr13/di/bpm-04': ['sr13/di/bpm-acq-04', 'sr12/pc/corh-05', 'sr09/pc/corv-02', 'sr13/pc/qh05', 93],
                      'sr13/di/bpm-05': ['sr13/di/bpm-acq-05', 'sr12/pc/corh-05', 'sr09/pc/corv-02', 'sr13/pc/qh05', 94],
                      'sr13/di/bpm-06': ['sr13/di/bpm-acq-06', 'sr04/pc/corh-05', 'sr09/pc/corv-02', 'sr13/pc/qh06', 95],
                      'sr13/di/bpm-07': ['sr13/di/bpm-acq-07', 'sr08/pc/corh-05', 'sr16/pc/corv-04', 'sr13/pc/qv02', 96],
                      'sr14/di/bpm-01': ['sr14/di/bpm-acq-01', 'sr13/pc/corh-01', 'sr05/pc/corv-02', 'sr14/pc/qv03', 97],
                      'sr14/di/bpm-02': ['sr14/di/bpm-acq-02', 'sr13/pc/corh-01', 'sr05/pc/corv-02', 'sr14/pc/qh07', 98],
                      'sr14/di/bpm-03': ['sr14/di/bpm-acq-03', 'sr13/pc/corh-01', 'sr09/pc/corv-02', 'sr14/pc/qh08', 99],
                      'sr14/di/bpm-04': ['sr14/di/bpm-acq-04', 'sr13/pc/corh-01', 'sr09/pc/corv-02', 'sr14/pc/qh08', 100],
                      'sr14/di/bpm-05': ['sr14/di/bpm-acq-05', 'sr13/pc/corh-01', 'sr16/pc/corv-04', 'sr14/pc/qh09', 101],
                      'sr14/di/bpm-06': ['sr14/di/bpm-acq-06', 'sr13/pc/corh-01', 'sr13/pc/corv-02', 'sr14/pc/qh09', 102],
                      'sr14/di/bpm-07': ['sr14/di/bpm-acq-07', 'sr09/pc/corh-01', 'sr13/pc/corv-02', 'sr14/pc/qh10', 103],
                      'sr14/di/bpm-08': ['sr14/di/bpm-acq-08', 'sr13/pc/corh-01', 'sr04/pc/corv-04', 'sr14/pc/qv04', 104],
                      'sr15/di/bpm-01': ['sr15/di/bpm-acq-01', 'sr16/pc/corh-05', 'sr09/pc/corv-02', 'sr15/pc/qv04', 105],
                      'sr15/di/bpm-02': ['sr15/di/bpm-acq-02', 'sr04/pc/corh-05', 'sr16/pc/corv-04', 'sr15/pc/qh10', 106],
                      'sr15/di/bpm-03': ['sr15/di/bpm-acq-03', 'sr16/pc/corh-05', 'sr16/pc/corv-04', 'sr15/pc/qh09', 107],
                      'sr15/di/bpm-04': ['sr15/di/bpm-acq-04', 'sr16/pc/corh-05', 'sr13/pc/corv-02', 'sr15/pc/qh09', 108],
                      'sr15/di/bpm-05': ['sr15/di/bpm-acq-05', 'sr16/pc/corh-05', 'sr04/pc/corv-04', 'sr15/pc/qh08', 109],
                      'sr15/di/bpm-06': ['sr15/di/bpm-acq-06', 'sr16/pc/corh-05', 'sr04/pc/corv-04', 'sr15/pc/qh08', 110],
                      'sr15/di/bpm-07': ['sr15/di/bpm-acq-07', 'sr14/pc/corh-04', 'sr08/pc/corv-04', 'sr15/pc/qh07', 111],
                      'sr15/di/bpm-08': ['sr15/di/bpm-acq-08', 'sr16/pc/corh-05', 'sr01/pc/corv-02', 'sr15/pc/qv03', 112],
                      'sr16/di/bpm-01': ['sr16/di/bpm-acq-01', 'sr05/pc/corh-01', 'sr13/pc/corv-02', 'sr16/pc/qv02', 113],
                      'sr16/di/bpm-02': ['sr16/di/bpm-acq-02', 'sr09/pc/corh-01', 'sr04/pc/corv-04', 'sr16/pc/qh06', 114],
                      'sr16/di/bpm-03': ['sr16/di/bpm-acq-03', 'sr08/pc/corh-05', 'sr04/pc/corv-04', 'sr16/pc/qh05', 115],
                      'sr16/di/bpm-04': ['sr16/di/bpm-acq-04', 'sr01/pc/corh-01', 'sr04/pc/corv-04', 'sr16/pc/qh04', 116],
                      'sr16/di/bpm-05': ['sr16/di/bpm-acq-05', 'sr05/pc/corh-01', 'sr08/pc/corv-04', 'sr16/pc/qh03', 117],
                      'sr16/di/bpm-06': ['sr16/di/bpm-acq-06', 'sr08/pc/corh-05', 'sr08/pc/corv-04', 'sr16/pc/qh02', 118],
                      'sr16/di/bpm-07': ['sr16/di/bpm-acq-07', 'sr08/pc/corh-05', 'sr01/pc/corv-02', 'sr16/pc/qv01', 119]}
