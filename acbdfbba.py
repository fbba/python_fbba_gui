#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Created on: 26.06.2018
Author: E.Morales
Based on Z.Marti FBBA work in Matlab.
Supervised by Z.Marti and F.Fernandez.
Copyright 2018 CELLS / ALBA Synchrotron, Cerdanyola del Valles, Spain
"""
import sys
import numpy as np
import os
import matplotlib.pyplot as plt
from taurus.external.qt import QtGui, QtCore, uic
import taurus
from fandango import tango
import acbdConstantsFBBA
import time
import datetime
import traceback
import scipy.io as sio
import shutil
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

sys.path.append("/homelocal/sicilia/applications/fa-archiver/falib")
import falib

form_bpm = uic.loadUiType("/control/user-packages/acbdfbba_module/ui/bpmSelector2.ui")[0]
form_class_offsets = uic.loadUiType("/control/user-packages/acbdfbba_module/ui/bpmOffset.ui")[0]
form_class = uic.loadUiType("/control/user-packages/acbdfbba_module/ui/fbba.ui")[0]


class fbbaMainClass(QtGui.QWidget, form_class):
    def __init__(self, parent=None):

        QtGui.QWidget.__init__(self, parent)

        self.setupUi(self)
        self.setWindowTitle("FBBA")

        # Init the UI at first run:
        self.uiConfig()

        # Start log:
        self.logFile = self.logFolderCreator()

        # Connect Signals and buttons with functions.
        self.cb_select.currentIndexChanged.connect(self.checkCombo)
        self.chb_ac.stateChanged.connect(self.chbToggleAc)
        self.chb_dc.stateChanged.connect(self.chbToggleDc)
        self.pb_bpms.clicked.connect(self.launchBpms)
        self.pb_start.clicked.connect(self.startMeasure)
        self.pb_stop.clicked.connect(self.stopRoutine)
        self.pb_plot.clicked.connect(self.PlotDataButton)
        self.pb_apply.clicked.connect(self.applyOffsets)
        self.pb_saveOffsets.clicked.connect(self.saveOffsets)
        self.pb_recoverOffsets.clicked.connect(self.recoverOffsets)
        self.pb_change_ref.clicked.connect(self.chageRefFile)
        self.te_bpm_num.setText("ALL")

        # Global variables
        self.freq_ad = None
        self.bpm_sectors = None
        self.stop = False
        self.bpm_numbers = []
        self.bpmDS = []
        self.bpmsACQ = []
        self.bkupHCMWF = {}
        self.bkupVCMWF = {}
        self.bkupSKEWWF = {}
        self.HCM = []
        self.VCM = []
        self.bpmNumber = []
        self.QUADS = []
        self.SKEWS = []
        self.dataAnalized = []
        self.projectionDict = {}
        self.offsetsDict = {}
        self.savePath = "/data/SR/FBBA"
        self.actualOffsets = {}
        self.hour_filename = datetime.datetime.now().strftime("%H%M%S")
        self.day_filename = datetime.datetime.now().strftime("%Y%m%d")

    def uiConfig(self):
        """
        We config GUI start. All the push buttons and check boxes not needed 
        at the init are disabled.
        """

        self.chb_ac.setEnabled(False)
        self.chb_dc.setEnabled(False)
        self.pb_stop.setEnabled(False)
        self.pb_save.setEnabled(False)
        self.progressBar.setValue(0)

    def logFolderCreator(self):
        """
        Method to check and create log folder and file in today folder.
        :return:
        """

        # Checlk if today folder exists
        today = datetime.datetime.now().strftime("%Y%m%d")
        todayFolder = "/data/SR/" + today
        todayFolderBool = os.path.exists(todayFolder)

        # If not, we will create today folder.
        if todayFolderBool == False:
            os.mkdir(todayFolder)

        # Check if FFBALog folder exists and if ot we will create it.
        logFolder = todayFolder + "/FBBALog"
        logFolfderBool = os.path.exists(logFolder)

        if logFolfderBool == False:
            os.mkdir(logFolder)

        logFile = logFolder + "/log.txt"
        dateLog = datetime.datetime.now().strftime("%Y%m%d %H:%m:%S")
        with open(logFile, "a") as f:
            f.write(dateLog + "  Start FBBA GUI. \n")
        f.close()

        return logFile

    def logWriter(self, fname, text):
        """
        Method to write in a log file.
        :param fname: path to log file.
        :param text: Text to write.
        :return: Nothing.
        """

        dateText = datetime.datetime.now().strftime("%Y%m%d %H:%m:%S")
        toWrite = dateText + "  " + text + "\n"

        with open(fname, "a") as f:
            f.write(toWrite)
        f.close()

    def stopRoutine(self):
        """
        Method to stop the measurement.

        :return: Nothing
        """

        self.stop = True
        self.logWriter(self.logFile, "Stop measurement")

    def applyOffsets(self):
        """
        Method to launch the BPM plot and apply widget.

        :return: Nothing

        """
        self.logWriter(self.logFile, "Apply Offsets dialog launched")
        self.apply = bpmOffsets()
        self.apply.show()

    def checkCombo(self):
        """
        Method to check the FBBA Magnets Combo.
        We check combo box current index and assign to check boxes AC and DC a 
        pre-fixed configuration.

        :return: Nothing
        """

        if self.cb_select.currentIndex() == 0:
            pass
        else:
            if self.cb_select.currentIndex() == 2:
                self.chb_ac.setEnabled(True)
                self.chb_dc.setEnabled(True)
                self.chb_dc.setChecked(False)
                self.chb_ac.setChecked(False)
            elif self.cb_select.currentIndex() == 1:
                self.chb_ac.setEnabled(False)
                self.chb_ac.setChecked(False)
                self.chb_dc.setEnabled(True)
                self.chb_dc.setChecked(True)

    def chbToggleAc(self):
        """
        Method to uncheck the DC check box in case that AC checkbox is checked.

        :return: Nothing
        """

        if self.chb_ac.isChecked():
            self.chb_dc.setChecked(False)

    def chbToggleDc(self):
        """
        Method to uncheck the AC check box in case that DC checkbox is checked.

        :return: Nothing
        """
        if self.chb_dc.isChecked():
            self.chb_ac.setChecked(False)

    def launchBpms(self):
        """
        Method to launch the bpmClass to get SR sectors to measure and analyze.

        """
        self._popup = bpmClass(self.bpm_sectors)
        self._popup.show()
        if self._popup.exec_():

            self.bpm_sectors = self._popup.selected

        if self.bpm_sectors:
            # print self.bpm_sectors
            self.logWriter(self.logFile, "BPM selected correctly.")
        else:
            self.tb_console.append("Error: BPM list empty")
            self.logWriter(self.logFile, "Error: BPM list empty.")

    def checkSetting(self):
        """
        Method to check if all the previous measurement configuration is OK or 
        not.

        :return: Boolean that is True if all is OK and False if something 
        wrong.

        """

        aux = True
        self.bpm_numbers = self.getBPMNumbersQuads()
        if not self.bpm_sectors:
            self._error = errorDialog(1)
            self.logWriter(self.logFile, "Error: No BPM selected.")
            self._error.show()
            aux = False
        if self.cb_select.currentIndex() == 1 and not self.chb_dc.isChecked():
            self.logWriter(self.logFile, "Error: Quad measurement and no DC.")
            self._error = errorDialog(2)
            self._error.show()
            aux = False
        if self.cb_select.currentIndex() == 2:
            if not self.chb_ac.isChecked() and not self.chb_dc.isChecked():
                self.logWriter(self.logFile, "Error: Skew measurement and no AC or DC.")
                self._error = errorDialog(3)
                self._error.show()
                aux = False

        return aux

    def psSetpointChecker(self, devAttWrite, devAttRead, newPoint):
        """

        Method to write quadrupole settings. We will write a setting and check 
        if the setting is correctly applied.
        If the difference between real value and expected value is more than 
        0.3A we will write setting again.
        This operation will repeat up to 3 times.
        :param devAttWrite: Quadrupole device server to write new value
        :param devAttRead: Quadrupole device ser to read actual value.
        :param newPoint: New setpoint to apply to quadrupole.
        :return: True if setpoint is correctly applied and

        """

        out = False
        taurus.Attribute(devAttWrite).write(newPoint)
        time.sleep(1)

        retry = 3

        while retry > 0:
            current = taurus.Attribute(devAttRead).read().rvalue.magnitude
            diff = abs(current - newPoint)
            if diff < 0.3:
                retry = 0
                out = True
            else:
                taurus.Attribute(devAttWrite).write(newPoint)
                time.sleep(1)
                retry = retry - 1

        return out

    def getBPMNumbersQuads(self):
        """
        Method to extract BPM numbers of the text edit for Quads measurements.

        :keyword ALL: Used to perform a measurement of all BPMs in a sector/s.
        :keyword ORBIT: Used to perform a measurement only with th e orbit 
        related BPMs.

        Is possible to measure only a few BPMs. Only insert the numbers 
        separated by a comma.EX: '2,3,5'

        :return: List with the BPM numbers.

        """

        if str(self.te_bpm_num.toPlainText()) == "ALL":
            return ["1", "2", "3", "4", "5", "6", "7", "8"]
        else:
            if str(self.te_bpm_num.toPlainText()) == "ORBIT":
                return ["1", "2", "3", "6", "7", "8"]
            else:
                nums = str(self.te_bpm_num.toPlainText())
                return nums.split(",")

    def getNumbersSkews(self):
        """
        Method to extract the BPM number for SR SKEWS measuremnts.
        To take in account: Skew measurement only use BMPs 3 and 5/6 of each 
        sector. Other BPMs will be ignored.

        :keyword ALL: Used to perform a measurement of all BPMs in a sector/s.

        Is possible to measure only a few BPMs. Only insert the numbers 
        separated by a comma. EX: '6'

        :return: List with the BPM numbers.

        """
        if str(self.te_bpm_num.toPlainText()) == "ALL":
            return ["3", "5", "6"]
        else:
            if str(self.te_bpm_num.toPlainText()) == "ORBIT":
                pass
            else:
                nums = str(self.te_bpm_num.toPlainText())
                nums = nums.split(",")
                # print nums
                nums2 = []
                for a in nums:
                    if a == "3":
                        nums2.append(a)
                    else:
                        if a == "5":
                            nums2.append(a)
                        else:
                            if a == "6":
                                nums2.append(a)
                            else:
                                pass
                return nums2

    def getBPMListQuads(self):
        """
        Method to create BPMs and Corrector lists to work with. This method is 
        used to perform a measurement with QUADS.

        :return: 4 lists with the BPMs DS names ,BPMs ACQ DS names, HCM DS 
        names and VCM DS names.

        """
        bpmDS = []

        self.tb_console.append("Getting BPM List for Quad measurement...")
        self.logWriter(self.logFile, "Getting BPM List for Quad measurement.")
        app.processEvents()

        if len(self.bpm_numbers) > 7:
            for i in self.bpm_sectors:
                if (
                    i == "sr02"
                    or i == "sr03"
                    or i == "sr06"
                    or i == "sr07"
                    or i == "sr10"
                    or i == "sr11"
                    or i == "sr14"
                    or i == "sr15"
                ):
                    for j in self.bpm_numbers:
                        bpmDS.append((i + "/di/bpm-0" + j))
                else:
                    for j in range(7):
                        bpmDS.append((i + "/di/bpm-0" + str(j + 1)))

        if len(self.bpm_numbers) > 5 and len(self.bpm_numbers) < 7:
            for i in self.bpm_sectors:
                if (
                    i == "sr02"
                    or i == "sr03"
                    or i == "sr06"
                    or i == "sr07"
                    or i == "sr10"
                    or i == "sr11"
                    or i == "sr14"
                    or i == "sr15"
                ):
                    for j in self.bpm_numbers:
                        bpmDS.append((i + "/di/bpm-0" + j))
                else:
                    list = ["1", "2", "3", "6", "7"]
                    for j in list:
                        bpmDS.append((i + "/di/bpm-0" + j))

        if len(self.bpm_numbers) < 5:
            for i in self.bpm_sectors:
                for j in self.bpm_numbers:
                    if (
                        i == "sr02"
                        or i == "sr03"
                        or i == "sr06"
                        or i == "sr07"
                        or i == "sr10"
                        or i == "sr11"
                        or i == "sr14"
                        or i == "sr15"
                    ):
                        bpmDS.append((i + "/di/bpm-0" + j))
                    else:
                        if j == "8":
                            pass
                        else:
                            bpmDS.append((i + "/di/bpm-0" + j))

        self.logWriter(self.logFile, "End BPM List for Quad measurement.")
        return bpmDS

    def getBPMListSkews(self):
        """
        Method used to create a BPM list to work in case of a skew measurement 
        selected.

        :return: List with the BPMs DS names.

        """
        self.tb_console.append("Getting BPM List for Skew measurement...")
        self.logWriter(self.logFile, "Getting BPM List for Skew measurement.")
        app.processEvents()
        bpmDS = []
        for i in self.bpm_sectors:
            if (
                i == "sr02"
                or i == "sr03"
                or i == "sr06"
                or i == "sr07"
                or i == "sr10"
                or i == "sr11"
                or i == "sr14"
                or i == "sr15"
            ):
                for j in self.bpm_numbers:
                    if j == "3":
                        bpmDS.append((i + "/di/bpm-0" + j))
                    else:
                        if j == "6":
                            bpmDS.append((i + "/di/bpm-0" + j))
                        else:
                            pass
            else:
                for j in self.bpm_numbers:
                    if j == "3":
                        bpmDS.append((i + "/di/bpm-0" + j))
                    else:
                        if j == "5":
                            bpmDS.append((i + "/di/bpm-0" + j))
                        else:
                            pass
        self.logWriter(self.logFile, "End BPM List for Skew measurement.")
        return bpmDS

    def listSkews(self):
        """
        Method to get the SR skews.

        :return: Lists with all SR skews.

        """

        qsk = tango.get_matching_devices("sr*/pc/qs*")

        return qsk

    def bkupCorrWF(self, corrH, corrV, skew=None):
        """
        Method that mantains a backup of the WVFselected attribute from the SR 
        correctors in order to recover once
        the mesaurement is done.

        :param corrH: List of HCM used. We will do the backup ONLY in this 
        ones.
        :param corrV: List of VCM used. We will do the backup ONLY in this 
        ones.
        :param skew: List of Skew Quads used. We will do the backup ONLY in
         this ones.

        :return: Nothing

        """
        self.logWriter(self.logFile, "Corrector Backup creation.")

        new_corrH = self.reduceCorr(corrH)
        new_corrV = self.reduceCorr(corrV)

        try:
            for i in new_corrH:
                a = taurus.Attribute(i + "/WVFSelected").read().rvalue
                time.sleep(0.01)
                b = taurus.Attribute(i + "/WVFScaling").read().rvalue
                time.sleep(0.01)
                c = taurus.Attribute(i + "/WVFStepSize").read().rvalue
                # time.sleep(0.1)
                self.bkupHCMWF[i] = [a.magnitude, b.magnitude, c.magnitude]
        except:
            self.tb_console.append("Problems doing the HCM WF backup: " + str(i))
            app.processEvents()
            self.tb_console.append(traceback.print_exc())

        try:
            for j in new_corrV:
                a = taurus.Attribute(j + "/WVFSelected").read().rvalue
                time.sleep(0.01)
                b = taurus.Attribute(j + "/WVFScaling").read().rvalue
                time.sleep(0.01)
                c = taurus.Attribute(j + "/WVFStepSize").read().rvalue
                # time.sleep(0.1)
                self.bkupVCMWF[j] = [a.magnitude, b.magnitude, c.magnitude]
        except:
            self.tb_console.append("Problems doing the VCM WF backup: " + str(j))
            app.processEvents()
            self.tb_console.append(traceback.print_exc())

        if skew:
            try:
                for k in skew:
                    a = taurus.Attribute(k + "/WVFSelected").read().rvalue
                    time.sleep(0.01)
                    b = taurus.Attribute(k + "/WVFScaling").read().rvalue
                    time.sleep(0.01)
                    c = taurus.Attribute(k + "/WVFStepSize").read().rvalue
                    # time.sleep(0.01)
                    self.bkupSKEWWF[k] = [a.magnitude, b.magnitude, c.magnitude]
            except:
                self.tb_console.append("Problems doing the SKEW WF backup: " + str(k))
                self.tb_console.append(traceback.print_exc())

        self.logWriter(self.logFile, "Corrector Backup creation done.")

    # def restoreCorrWF(self, corrH, corrV):
    def restoreCorrWF(self, dictCorr):
        """
        Method to recover WVFselected to the value we had at the start of the 
        measurement. FRM waveform selected.

        :param corrH: List of HCM used in the measurement. We will restore 
        ONLY this ones.
        :param corrV: List of VCM used in the measurement. We will restores 
        ONLY this ones.

        :return: Return 2 booleans that will be false in case of error or 
        discordance.

        """
        corrOK = False
        attributes = ["/WVFSelected", "/WVFScaling", "/WVFStepSize"]

        self.tb_console.append("Recovering Correctors Waveforms...")
        self.logWriter(self.logFile, "Recovering Correctors Waveforms...")
        app.processEvents()

        keyH = dictCorr.keys()

        for i in keyH:
            try:
                taurus.Attribute(i + attributes[0]).write(float(dictCorr[i][0]))
                time.sleep(0.3)
            except:
                self.tb_console.append("Problems restoring WVFSelected: " + str(i))
                self.logWriter(
                    self.logFile, "Problems restoring WVFSelected: " + str(i)
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

        for j in keyH:
            try:
                taurus.Attribute(j + attributes[1]).write(float(dictCorr[j][1]))
                time.sleep(0.3)
            except:
                self.tb_console.append("Problems restoring WVFScaling: " + str(j))
                self.logWriter(self.logFile, "Problems restoring WVFScaling: " + str(j))
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

        for k in keyH:
            try:
                taurus.Attribute(k + attributes[2]).write(float(dictCorr[k][2]))
                time.sleep(0.3)
            except:
                self.tb_console.append("Problems restoring WVFStepSize: " + str(k))
                self.logWriter(
                    self.logFile, "Problems restoring WVFStepSize: " + str(k)
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

        corrOK = True
        self.tb_console.append("Correctors restored!")
        self.logWriter(self.logFile, "Correctors restored!")
        app.processEvents()

        return corrOK

    def writeCorrWF(self, corr, nwave):
        """
        Method to write the nwave in to a corrector DS.

        :param corr: Single HCM DS names.
        :param nwave: Number of the wave to write in the corrector DS.

        :return: Nothing

        """
        try:
            taurus.Attribute(corr + "/WVFselected").write(nwave)
            time.sleep(0.3)
        except:
            self.tb_console.append(
                "Problems wrinting WVFselected in corrector: " + corr
            )
            self.logWriter(
                self.logFile, "Problems wrinting WVFselected in corrector: " + corr
            )
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

    def checkCorrWF(self, corrH, corrV, nwave):
        """
        Method to check if the WVFseleceted in corrector DS is the waveform we 
        need to permorm the masurements.

        :param corrH: List of HCM to check.
        :param corrV: List of VCM to check.
        :param nwave: Number of WVFselected expected.

        :return: True if all the WVFselected is equal to nwave and False if one 
        or more are different.

        """
        auxH = []
        auxV = []

        new_corrV = self.reduceCorr(corrV)
        new_corrH = self.reduceCorr(corrH)

        for i in new_corrV:
            try:
                a = taurus.Attribute(i + "/WVFselected").read().rvalue
                auxV.append(a.magnitude)
                time.sleep(0.01)
            except:
                self.tb_console.append(
                    "Problems reading WVFselected in corrector: " + i
                )
                self.logWriter(
                    self.logFile, "Problems reading WVFselected in corrector: " + i
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

        for j in new_corrH:
            try:
                b = taurus.Attribute(j + "/WVFselected").read().rvalue
                auxH.append(b.magnitude)
                time.sleep(0.01)
            except:
                self.tb_console.append(
                    "Problems reading WVFselected in corrector: " + j
                )
                self.logWriter(
                    self.logFile, "Problems reading WVFselected in corrector: " + j
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

        if all(auxH) == int(nwave) and all(auxV) == int(nwave):
            return True
        else:
            return False

    def changeSRCorrStepSize(self, corr, WVFStepsize):
        """
        Method to change the WVFSteptepSize of one corrector. In case the 
        WVFStepsize < 80 a ValueError exception will be raised.

        :param corr: Corrector (HCM or VCM) used in the measurement
        :param WVFStepsize: StepSize to write in the corrector.

        :return: Nothing

        """
        if int(WVFStepsize) >= 80:
            try:
                taurus.Attribute(corr + "/WVFStepSize").write(float(WVFStepsize))
            except:
                self.tb_console.append(
                    "Problems to set the WVFStepSize in corrector/skew: " + corr
                )
                self.logWriter(
                    self.logFile,
                    "Problems to set the WVFStepSize in corrector/skew: " + corr,
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()
        else:
            raise ValueError("The WVFStepSize is not correct.")

    def setcorrWVFAutoTrigger(self, corr):
        """
        Method to set the WVFAutoTrigger in every corrector used.

        :param corr: Corrector (HCM or VCM) used in the measurement.

        :return: Nothing

        """
        try:
            taurus.Attribute(corr + "/WVFAutoTrigger").write(True)
        except:
            self.tb_console.append("Problems setting the WVFAutoTrigger")
            self.logWriter(self.logFile, "Problems setting the WVFAutoTrigger")
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

    def setWVFScaling(self, corr, WVFScaling):
        """
        Method to write the WVFScaling attribute.

        :param corr: Corrector (HCM or VCM) used in the measurement.
        :param WVFScaling: Value for the WVFScaling.

        :return: Nothing

        """
        try:
            taurus.Attribute(corr + "/WVFScaling").write(float(WVFScaling))
        except:
            self.tb_console.append("Problems writting WVFScaling in corrector: " + corr)
            self.logWriter(
                self.logFile, "Problems writting WVFScaling in corrector: " + corr
            )
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

    def reduceCorr(self, corr):
        """
        Method to reduce redundant correctors in a correctors list.
        :param corr: List of correctors
        :return: List of correctors without 2 equal elements.
        """

        new_corr = []
        for i in corr:
            if i in new_corr:
                continue
            else:
                new_corr.append(i)

        return new_corr

    def corrWVFPrepare(self, corr, wvfNum, WVFScaling, WVFStepsize):
        """
        Method to prepare a corrector for the measurement.

        :param corr: Corrector (HCM or VCM) to prepare.
        :param wvfNum: Number of the internal WVF use din the measurement.
        :param WVFScaling: Value of the WFV scaling factor.
        :param WVFStepsize: Value of the step size of the WVF.

        :return: Nothing
        """

        new_corr = self.reduceCorr(corr)

        try:

            for i in new_corr:
                self.changeSRCorrStepSize(i, float(WVFStepsize))
                time.sleep(0.3)
            for i in new_corr:
                self.setcorrWVFAutoTrigger(i)
                time.sleep(0.3)
            for i in new_corr:
                self.writeCorrWF(i, float(wvfNum))
                time.sleep(0.3)
            for i in new_corr:
                self.setWVFScaling(i, float(WVFScaling))
                time.sleep(0.3)

        except:
            self.tb_console.append("Problems preparing the corrector/skew: " + i)
            self.logWriter(self.logFile, "Problems preparing the corrector/skew: " + i)
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

    def corrWVFStop(self, corrH, corrV, skew=None):
        """
        Method to Stop waveform in a corrector or in a list of a correctors 
        used in measurement.
        The method allow user to:
        - Use method only to stop 1 corrector (one HCM and one VCM).
        - Use method to stop a list of HCM and VCM.

        :param corrH: Lists of HCM or simple HCM used in the measurement.
        :param corrV: List of VCM or simple VCM used in the measurement.
        :param skew: List of SKEWS or simple SKEW used for the AC measurement.
         Default None.

        :return: Nothing

        """
        if isinstance(corrV, list) and isinstance(corrH, list):
            new_corrH = self.reduceCorr(corrH)
            new_corrV = self.reduceCorr(corrV)
            for i in new_corrH:
                try:
                    deviceH = taurus.Device(i)
                    deviceH.WVFStop(False)
                    time.sleep(0.1)
                except:
                    self.tb_console.append("Problems with WVFStop in corrector: " + i)
                    self.logWriter(
                        self.logFile, "Problems with WVFStop in corrector: " + i
                    )
                    self.tb_console.append(traceback.print_exc())
                    app.processEvents()

            for j in new_corrV:
                try:
                    deviceV = taurus.Device(j)
                    deviceV.WVFStop(False)
                    time.sleep(0.1)
                except:
                    self.tb_console.append("Problems with WVFStop in corrector: " + j)
                    self.logWriter(
                        self.logFile, "Problems with WVFStop in corrector: " + j
                    )
                    self.tb_console.append(traceback.print_exc())
                    app.processEvents()
        else:
            try:
                deviceH = taurus.Device(corrH)
                deviceV = taurus.Device(corrV)
                deviceH.WVFStop(False)
                time.sleep(0.1)
                deviceV.WVFStop(False)
            except:
                self.tb_console.append(
                    "Problems with WVFStop in corrector: " + corrH + ", " + corrV
                )
                self.logWriter(
                    self.logFile,
                    "Problems with WVFStop in corrector: " + corrH + ", " + corrV,
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

        if skew:
            if (
                isinstance(corrV, list)
                and isinstance(corrH, list)
                and isinstance(skew, list)
            ):
                new_corrH = self.reduceCorr(corrH)
                new_corrV = self.reduceCorr(corrV)
                for i in new_corrH:
                    try:
                        deviceH = taurus.Device(i)
                        deviceH.WVFStop(False)
                        time.sleep(0.1)
                    except:
                        self.tb_console.append(
                            "Problems with WVFStop in corrector: " + i
                        )
                        self.logWriter(
                            self.logFile, "Problems with WVFStop in corrector: " + i
                        )
                        self.tb_console.append(traceback.print_exc())
                        app.processEvents()

                for j in new_corrV:
                    try:
                        deviceV = taurus.Device(j)
                        deviceV.WVFStop(False)
                        time.sleep(0.1)
                    except:
                        self.tb_console.append(
                            "Problems with WVFStop in corrector: " + j
                        )
                        self.logWriter(
                            self.logFile, "Problems with WVFStop in corrector: " + j
                        )
                        self.tb_console.append(traceback.print_exc())
                        app.processEvents()

                for k in skew:
                    try:
                        deviceV = taurus.Device(k)
                        deviceV.WVFStop(False)
                        time.sleep(0.1)
                    except:
                        self.tb_console.append("Problems with WVFStop in Skew: " + k)
                        self.logWriter(
                            self.logFile, "Problems with WVFStop in Skew: " + k
                        )
                        self.tb_console.append(traceback.print_exc())
                        app.processEvents()
            else:
                try:
                    deviceH = taurus.Device(corrH)
                    deviceV = taurus.Device(corrV)
                    skew = taurus.Device(skew)
                    deviceH.WVFStop(False)
                    time.sleep(0.01)
                    deviceV.WVFStop(False)
                    time.sleep(0.01)
                    skew.WVFStop(False)
                    time.sleep(0.01)
                except:
                    self.tb_console.append(
                        "Problems with WVFStop in corrector: "
                        + corrH
                        + ", "
                        + corrV
                        + ", "
                        + skew
                    )
                    self.logWriter(
                        self.logFile,
                        "Problems with WVFStop in corrector: "
                        + corrH
                        + ", "
                        + corrV
                        + ", "
                        + skew,
                    )
                    self.tb_console.append(traceback.print_exc())
                    app.processEvents()

    def corrStartWVF(self, corrH, corrV, skew=None):
        """
        Method to start a ramp in a magnet. We can start a ramp in a HCM, VCM 
        or Skew quadrupole.

        :param corrH: HCM corrector used in the measurement.
        :param corrV: VCM corrector used in the measurement.

        :return: Nothing

        """
        try:
            deviceH = taurus.Device(corrH)
            deviceH.WVFStart(True)
        except:
            self.tb_console.append("Problems Starting the corrector ramp in: " + corrH)
            self.logWriter(
                self.logFile, "Problems Starting the corrector ramp in: " + corrH
            )
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

        try:
            deviceV = taurus.Device(corrV)
            deviceV.WVFStart(True)
        except:
            self.tb_console.append("Problems Starting the corrector ramp in: " + corrV)
            self.logWriter(
                self.logFile, "Problems Starting the corrector ramp in: " + corrV
            )
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

        if skew:
            try:
                skewds = taurus.Device(skew)
                skewds.WVFStart(True)
            except:
                self.tb_console.append("Problems Starting the Skew ramp in: " + skew)
                self.logWriter(
                    self.logFile, "Problems Starting the Skew ramp in: " + skew
                )
                self.tb_console.append(traceback.print_exc())
                app.processEvents()

    def prepareFBBA(self, corrH, corrV):
        """
        Method to configure HCM and VCM to perform FBBA measurement.

        :param corrH: List of HCM used in measurement.
        :param corrV: List of VCM used in the measurement.

        """
        self.tb_console.append("Preparing FBBA...")
        self.logWriter(self.logFile, "Preparing FBBA...")
        app.processEvents()

        try:
            self.bkupCorrWF(corrH, corrV)
            self.logWriter(self.logFile, "Corrector backup created.")
            self.corrWVFStop(corrH, corrV)
            self.corrWVFPrepare(
                corrH,
                acbdConstantsFBBA.wvf_num,
                acbdConstantsFBBA.WvFScaling,
                acbdConstantsFBBA.WVFStepSize,
            )
            self.logWriter(self.logFile, "Horizontal correctors prepared.")
            self.corrWVFPrepare(
                corrV,
                acbdConstantsFBBA.wvf_num,
                acbdConstantsFBBA.WvFScaling,
                acbdConstantsFBBA.WVFStepSize,
            )
            self.logWriter(self.logFile, "Vertical correctors prepared.")

            self.tb_console.append("Preparation done.")
            self.logWriter(self.logFile, "Preparation done.")
            self.tb_console.append("Wait next steps...")
            app.processEvents()

        except:

            self.tb_console.append("Problems preparing FBBA")
            self.logWriter(self.logFile, "Problems preparing FBBA")
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

    def prepareFBBASkewAC(self, corrH, corrV, skew):
        """
        Method to prepare the FBBA measurtement for the skew AC mode.

        :param corrH: List of HCM used in measurement.
        :param corrV: List of VCM used in the measurement
        :param skew: List of SKEW used in the measurement

        """
        self.tb_console.append("Preparing FBBA for Skew AC measurement")
        self.logWriter(self.logFile, "Preparing FBBA for Skew AC measurement")
        app.processEvents()

        try:
            self.bkupCorrWF(corrH, corrV, skew)
            self.logWriter(self.logFile, "Corrector backup created.")
            self.corrWVFStop(corrH, corrV, skew)
            self.corrWVFPrepare(
                corrH,
                acbdConstantsFBBA.wvf_num,
                acbdConstantsFBBA.WvFScaling,
                acbdConstantsFBBA.WVFStepSize,
            )
            self.logWriter(self.logFile, "Horizontal correctors prepared.")
            self.corrWVFPrepare(
                corrV,
                acbdConstantsFBBA.wvf_num,
                acbdConstantsFBBA.WvFScaling,
                acbdConstantsFBBA.WVFStepSize,
            )
            self.logWriter(self.logFile, "Vertical correctors prepared.")
            self.corrWVFPrepare(
                skew,
                acbdConstantsFBBA.wvf_num_skew,
                acbdConstantsFBBA.WvScalingSkew,
                acbdConstantsFBBA.WVFStepSize,
            )
            self.logWriter(self.logFile, "Skew correctors prepared.")

            self.tb_console.append("Preparation Skew AC measurement done.")
            self.logWriter(self.logFile, "Preparation Skew AC measurement done.")
            app.processEvents()

        except:
            self.tb_console.append("Problems preparing FBBA for Sew AC mesurement")
            self.logWriter(
                self.logFile, "Problems preparing FBBA for Sew AC mesurement"
            )
            self.tb_console.append(traceback.print_exc())
            app.processEvents()

    def getMagnets(self, BpmDs):
        """
        Method to obtain the correctors and quads related to bpms selected. We 
        iterate a dictionary in acdbConstantsFBBA and obtain all lists.

        :param BpmDs: BPMs selected by the user in the GUI

        :return hcm: list with HCM.
        :return vcm: list of VCM.
        :return bpmacq: list of BPM-ACQ DS names.
        :return quad: list of QUADS related to measurements.

        """
        self.tb_console.append("Running Get Magnets...")
        self.logWriter(self.logFile, "Running Get Magnets.")
        app.processEvents()
        hcm = []
        vcm = []
        bpmacq = []
        quad = []
        number = []
        if self.cb_select.currentIndex() == 1:
            for i in BpmDs:
                a = acbdConstantsFBBA.quads_measure_dict[i]
                hcm.append(a[1])
                vcm.append(a[2])
                bpmacq.append(a[0])
                quad.append(a[3])
                number.append(a[4])
        if self.cb_select.currentIndex() == 2:
            for i in BpmDs:
                a = acbdConstantsFBBA.skew_measure_dict[i]
                hcm.append(a[1])
                vcm.append(a[2])
                bpmacq.append(a[0])
                quad.append(a[3])
                number.append(a[4])

        self.tb_console.append("End Get Magnets.")
        self.logWriter(self.logFile, "End Get Magnets.")
        self.tb_console.append("Wait next steps...")
        app.processEvents()

        return hcm, vcm, bpmacq, quad, number

    def readFA(self, tiempo):
        """
        Method to read all machine BPMs form fast archiver. Method done by 
        Joana.

        :param tiempo: Time of measurement.

        :return:  Fast archiver data.
        """
        server = falib.Server()
        self.freq_ad = server.sample_frequency

        N = int(round(tiempo * self.freq_ad))
        fa_id_list = server.get_fa_ids()
        BPMs = list()

        for element in fa_id_list:

            string = element[1]

            # The ones corresponding to section 17 are not BPMs from the 
            # storage ring

            if list(string)[0:5] == ["B", "P", "M", "S", "R"]:
                try:
                    section = int(list(string)[5]) * 10 + int(
                        list(string)[6]
                    )  # Section is in positions 5 and 6
                    number = int(list(string)[7]) * 10 + int(
                        list(string)[8]
                    )  # Number of BPM is in positions 7 and 8
                except:
                    section = 17
                    number = None
            else:
                section = 17
                number = None

            name = (section, number)
            BPMs.append(name)

        # Reorder list and get rid of BPMs which are not on the storage ring 
        # (Section 17)

        BPMs_list = sorted(range(len(BPMs)), key=lambda k: BPMs[k])
        BPMs_list = [index for index in BPMs_list if BPMs[index][0] != 17]

        subscription = server.subscription(range(1, 125))
        signals1 = subscription.read(int(N))
        subscription.close()
        x = signals1[:, BPMs_list, 0].T * 1e-6
        y = signals1[:, BPMs_list, 1].T * 1e-6

        return x, y

    def writeQuad(self, quad, quad_current, signo, corrH, corrV):
        """
        Method to write quad values safely. On 30.06.2020 niht shift, operator 
        infromed about a error reading a Quad.
        It was a fake communication error. This method try to avoid it.
        :param quad: Quad to change current
        :param quad_current: Quad value at start.
        :param signo: Operation sign. Values are '-', '+' or None:
            '-': To substract delta value from nominal value and write it.
            '+': To add delta value to nominal value and write it.
            None: To write nominal value (previously read)
        :param corrH: HCM corrector ramping during measurement. In case of 
        error, we will stop it.
        :param corrV: VCM corrector ramping during measurement. In case of 
        error, we will stop it.
        :return: Nothing
        """

        self.logWriter(self.logFile, "Writing quad: " + str(quad))
        try:

            loop = True
            count = 0

            while loop == True:

                if count < 5:

                    state = taurus.Attribute(quad + "/State").read().rvalue

                    if state.name != "FAULT":
                        # print 'Hemos leido ell estado'

                        # print quad_current - acbdConstantsFBBA.QuadDelta

                        if signo == "-":
                            toWrite = quad_current - acbdConstantsFBBA.QuadDelta
                        if signo == "+":
                            toWrite = quad_current + acbdConstantsFBBA.QuadDelta
                        if signo is None:
                            toWrite = quad_current

                        print(toWrite)
                        taurus.Attribute(quad + "/CurrentSetPoint").write(toWrite)

                        # if signo == '-':
                        #     taurus.Attribute(quad + '/CurrentSetPoint').write(
                        #         float(quad_current.magnitude) - float(acbdConstantsFBBA.QuadDelta))
                        # if signo == '+':
                        #     taurus.Attribute(quad + '/CurrentSetPoint').write(
                        #         float(quad_current.magnitude) + float(acbdConstantsFBBA.QuadDelta))
                        # if signo is None:
                        #     taurus.Attribute(quad + '/CurrentSetPoint').write(float(quad_current.magnitude))

                        # Check if value is applied:

                        self.tb_console.append("Checking value of: " + str(quad))
                        self.logWriter(self.logFile, "Checking value of: " + str(quad))
                        app.processEvents()

                        time.sleep(1)
                        aux = taurus.Attribute(quad + "/Current").read().rvalue
                        result = abs(toWrite - aux.magnitude)

                        if result < 0.2:
                            loop = False
                        else:
                            time.sleep(0.5)
                            count = count + 1
                    else:
                        time.sleep(1)
                        count = count + 1

                else:
                    raise ValueError("We can not write quad value: " + str(quad))

            self.logWriter(self.logFile, "End quad: " + str(quad))
        except:
            self.tb_console.append("Problems with " + str(quad))
            self.logWriter(self.logFile, "Problems with " + str(quad))
            app.processEvents()
            print("Problems with " + str(quad))
            self.corrWVFStop(corrH, corrV)

    def measurebpm(self, bpm, corrH, corrV, quad, nbpm):
        """
        Method to measure queadrupole FBBA. This method perform the measurement 
        and obtain BPM offset.

        :param bpm: Actual BPM measured.
        :param corrH: HCM to star to ramp.
        :param corrV: VCM to start to ramp.
        :param quad: Quad to move.
        :param nbpm: Number of BPM

        """
        tiempo = 1.5

        self.tb_console.append("Start Measure " + str(bpm))
        self.logWriter(self.logFile, "Start Measure " + str(bpm))
        app.processEvents()

        quad_current = taurus.Attribute(quad + "/CurrentSetPoint").read().rvalue
        print(quad_current.magnitude)
        print(type(quad_current.magnitude))
        self.corrStartWVF(corrH, corrV)
        self.writeQuad(quad, quad_current.magnitude, "-", corrH, corrV)
        # taurus.Attribute(quad + '/CurrentSetPoint').write(
        #     float(quad_current.magnitude) - float(acbdConstantsFBBA.QuadDelta))
        time.sleep(1.5)
        dataMinusDeltaX, dataMinusDeltaY = self.readFA(tiempo)
        self.writeQuad(quad, quad_current.magnitude, "+", corrH, corrV)
        # taurus.Attribute(quad + '/CurrentSetPoint').write(
        #     float(quad_current.magnitude) + float(acbdConstantsFBBA.QuadDelta))
        time.sleep(1.5)
        dataPlusDeltaX, dataPlusDeltaY = self.readFA(tiempo)
        self.corrWVFStop(corrH, corrV)
        self.writeQuad(quad, quad_current.magnitude, None, corrH, corrV)
        # taurus.Attribute(quad + '/CurrentSetPoint').write(quad_current.magnitude)

        fs = self.freq_ad
        fh = acbdConstantsFBBA.fh
        fv = acbdConstantsFBBA.fv

        self.tb_console.append("Data colected from " + str(bpm))
        self.logWriter(self.logFile, "Data colected from " + str(bpm))
        app.processEvents()
        # self.tb_console.append('Saving RAW data...')
        #
        # # Save RAW data NPY:

        name_bpm = bpm.replace("/", "_")
        name_bpm = name_bpm.replace("-", "_")

        #np.save('/data/test/emorales/python/fbba_python/SaveData/RawDataNPY/RAW_Data_QUAD' + name_bpm + '.npy',
                #[dataMinusDeltaX, dataPlusDeltaX, dataMinusDeltaY, dataPlusDeltaY])
        
        ## Save RAW data MAT:
        #sio.savemat('/data/test/emorales/python/fbba_python/SaveData/RawDataMAT/RAW_Data_QUAD' + name_bpm + '.mat',
                    #{'dataMinusDeltaX': dataMinusDeltaX, 'dataPlusDeltaX': dataPlusDeltaX,
                     #'dataMinusDeltaY': dataMinusDeltaY, 'dataPlusDeltaY': dataPlusDeltaY})

        # self.tb_console.append('Start to analyze data collected')

        resultsX_minus = self.getProjection(dataMinusDeltaX, [0, fh / fs, fv / fs])

        resultsX_plus = self.getProjection(dataPlusDeltaX, [0, fh / fs, fv / fs])

        resultsY_minus = self.getProjection(dataMinusDeltaY, [0, fv / fs, fh / fs])

        resultsY_plus = self.getProjection(dataPlusDeltaY, [0, fv / fs, fh / fs])

        # self.projectionDict[name_bpm] = [resultsX_minus, resultsX_plus, resultsY_minus, resultsY_plus]

        offsets = self.getOffsetsQuads(
            nbpm, resultsX_minus, resultsX_plus, resultsY_minus, resultsY_plus
        )
        self.offsetsDict[name_bpm] = offsets

        self.saveData(self.hour_filename, self.day_filename)

        self.tb_console.append("BPM " + str(bpm) + ": Data analyzed and saved")
        self.logWriter(self.logFile, "BPM " + str(bpm) + ": Data analyzed and saved")
        app.processEvents()

    def measurebpmSkewDC(self, bpm, corrH, corrV, skew, nbpm):
        """
        Method to measure Skew DC FBBA. This method perform the measurement and 
        obtain BPM offset.
        :param bpm: BPM to measure
        :param corrH: HCM to ramp
        :param corrV: VCM to ramp
        :param skew: Skew magnet to move
        :param nbpm: BPM number
        :return: Nothing

        """
        tiempo = 6

        self.tb_console.append("Start Measure Skew DC in: " + str(bpm))
        self.logWriter(self.logFile, "Start Measure Skew DC in: " + str(bpm))
        app.processEvents()

        skew_current = taurus.Attribute(skew + "/CurrentSetPoint").read().rvalue
        self.corrStartWVF(corrH, corrV)
        self.writeQuad(skew, skew_current.magnitude, "-", corrH, corrV)
        # taurus.Attribute(skew + '/CurrentSetPoint').write(
        #     float(quad_current.magnitude) - float(acbdConstantsFBBA.QuadDelta))
        time.sleep(1.5)
        dataMinusDeltaX, dataMinusDeltaY = self.readFA(tiempo)
        self.writeQuad(skew, skew_current.magnitude, "+", corrH, corrV)
        # taurus.Attribute(skew + '/CurrentSetPoint').write(
        #     float(quad_current.magnitude) + float(acbdConstantsFBBA.QuadDelta))
        time.sleep(1.5)
        dataPlusDeltaX, dataPlusDeltaY = self.readFA(tiempo)
        self.writeQuad(skew, skew_current.magnitude, None, corrH, corrV)
        # taurus.Attribute(skew + '/CurrentSetPoint').write(skew_current.magnitude)
        self.corrWVFStop(corrH, corrV)

        fs = self.freq_ad
        fh = acbdConstantsFBBA.fh
        fv = acbdConstantsFBBA.fv

        self.tb_console.append("Data colected from " + str(bpm))
        self.logWriter(self.logFile, "Data colected from " + str(bpm))
        app.processEvents()

        # self.tb_console.append('Saving RAW data...')
        # # Save RAW data NPY:
        name_bpm = bpm.replace("/", "_")
        name_bpm = name_bpm.replace("-", "_")
        # np.save('/data/test/emorales/python/fbba_python/SaveData/RawDataNPY/RAW_Data_SKEWDC' + name_bpm + '.npy',
        #         [dataMinusDeltaX, dataPlusDeltaX, dataMinusDeltaY, dataPlusDeltaY])
        # # time.sleep(1.5)
        #
        # # Save RAW data MAT:
        # sio.savemat(
        #     '/data/test/emorales/python/fbba_python/SaveData/RawDataMAT/RAW_Data_SKEWDC' + name_bpm + '.mat',
        #     {'dataMinusDeltaX': dataMinusDeltaX, 'dataPlusDeltaX': dataPlusDeltaX,
        #      'dataMinusDeltaY': dataMinusDeltaY, 'dataPlusDeltaY': dataPlusDeltaY})
        # # time.sleep(1.5)

        self.tb_console.append("Start to analyze data collected for SKEW DC")
        self.logWriter(self.logFile, "Start to analyze data collected for SKEW DC")
        app.processEvents()

        resultsX_minus = self.getProjection(
            dataMinusDeltaX, [0, fh / fs, fv / fs], True
        )
        resultsX_plus = self.getProjection(dataPlusDeltaX, [0, fh / fs, fv / fs], True)
        resultsY_minus = self.getProjection(
            dataMinusDeltaY, [0, fv / fs, fh / fs], True
        )
        resultsY_plus = self.getProjection(dataPlusDeltaY, [0, fv / fs, fh / fs], True)

        self.projectionDict[name_bpm] = [
            resultsX_minus,
            resultsX_plus,
            resultsY_minus,
            resultsY_plus,
        ]

        offsets = self.getOffsetsSKEWSDC(
            nbpm, resultsX_minus, resultsX_plus, resultsY_minus, resultsY_plus
        )
        self.offsetsDict[name_bpm] = offsets

        # self.saveData(self.hour_filename, self.day_filename)

        self.tb_console.append("BPM " + str(bpm) + ": Data analyzed and saved")
        self.logWriter(self.logFile, "BPM " + str(bpm) + ": Data analyzed and saved")
        app.processEvents()

    def measurebpmSkewAC(self, bpm, corrH, corrV, skew, nbpm):
        """
        Method to measure Skew AC FBBA. This method perform the measurement and 
        obtain BPM offset.

        :param bpm: BPM to measure
        :param corrH: HCM to ramp.
        :param corrV: VCM to ramp
        :param skew: Skew magnet to ramp
        :param nbpm: BPM number

        :return: Nothing
        """
        tiempo = 6
        self.tb_console.append("Start Measure Skew AC in: " + str(bpm))
        self.logWriter(self.logFile, "Start Measure Skew AC in: " + str(bpm))
        app.processEvents()

        self.corrStartWVF(corrH, corrV, skew)

        dataX, dataY = self.readFA(tiempo)

        self.corrWVFStop(corrH, corrV, skew)

        fs = self.freq_ad
        fh = acbdConstantsFBBA.fh
        fv = acbdConstantsFBBA.fv
        fskew = acbdConstantsFBBA.fskew

        self.tb_console.append("Skew AC Data colected from " + str(bpm))
        self.logWriter(self.logFile, "Skew AC Data colected from " + str(bpm))
        app.processEvents()

        # self.tb_console.append('Saving RAW data...')
        # # Save RAW data NPY:
        name_bpm = bpm.replace("/", "_")
        name_bpm = name_bpm.replace("-", "_")
        # np.save('/data/test/emorales/python/fbba_python/SaveData/RawDataNPY/RAW_Data_SKEWAC_' + name_bpm + '.npy',
        #         [dataX, dataY])
        # # time.sleep(1.5)
        #
        # # Save RAW data MAT:
        # sio.savemat('/data/test/emorales/python/fbba_python/SaveData/RawDataMAT/RAW_Data_SKEWAC' + name_bpm + '.mat',
        #             {'dataX': dataX, 'dataY': dataY})
        # # time.sleep(1.5)

        self.tb_console.append("Start to analyze data collected for SKEW AC")
        self.logWriter(self.logFile, "Start to analyze data collected for SKEW AC")
        app.processEvents()

        resultsX = self.getProjection(
            dataX,
            [0, fh / fs, fv / fs, fskew / fs, (fv + fskew) / fs, abs(fv - fskew) / fs],
            True,
        )
        resultsY = self.getProjection(
            dataY,
            [0, fv / fs, fh / fs, fskew / fs, (fh + fskew) / fs, abs(fh - fskew) / fs],
            True,
        )

        self.projectionDict[name_bpm] = [resultsX, resultsY]

        offsets = self.getOffsetsSKEWSAC(nbpm, resultsX, resultsY)

        self.offsetsDict[name_bpm] = offsets

        # self.saveData(self.hour_filename, self.day_filename)

        self.tb_console.append("BPM " + str(bpm) + ": Data analyzed and saved")
        self.logWriter(self.logFile, "BPM " + str(bpm) + ": Data analyzed and saved")
        app.processEvents()

    def getOffsetsQuads(self, nbpm, dataXminus, dataXplus, dataYminus, dataYplus):
        """
        Method to obtain offset data for each BPM of QUADS Measurements.

        :param nbpm: BPM number (1 to 120)
        :param dataXminus: Projection data in horizontal plane with QUAD delta 
        minus.
        :param dataXplus: Projection data in horizontal plane with QUAD delta 
        plus.
        :param dataYminus: Projection data in vertical plane with QUAD delta
        minus.
        :param dataYplus: Projection data in vertical plane with QUAD delta 
        plus.

        :return: List with all the results obtained:[BPM_centre, BPM_error]

        """

        Axa = dataXminus[0]
        Mxa = dataXminus[1]
        Axb = dataXplus[0]
        Mxb = dataXplus[1]
        Aya = dataYminus[0]
        Mya = dataYminus[1]
        Ayb = dataYplus[0]
        Myb = dataYplus[1]

        Axa1 = np.zeros([3, 120])
        Axb1 = np.zeros([3, 120])
        Aya1 = np.zeros([3, 120])
        Ayb1 = np.zeros([3, 120])

        Axa1[0] = 2 * Axa[0, :] * np.sign(np.cos(Mxa[0, :]))
        Axb1[0] = 2 * Axb[0, :] * np.sign(np.cos(Mxb[0, :]))
        Aya1[0] = 2 * Aya[0, :] * np.sign(np.cos(Mya[0, :]))
        Ayb1[0] = 2 * Ayb[0, :] * np.sign(np.cos(Myb[0, :]))

        pha, phb = self.findphasesDC(Mxa[1, :], Mxb[1, :])
        pva, pvb = self.findphasesDC(Mya[1, :], Myb[1, :])

        Axa1[1] = 2 * Axa[1, :] * np.sign(np.cos(Mxa[1, :] - pha))
        Axb1[1] = 2 * Axb[1, :] * np.sign(np.cos(Mxb[1, :] - phb))
        Aya1[1] = 2 * Aya[1, :] * np.sign(np.cos(Mya[1, :] - pva))
        Ayb1[1] = 2 * Ayb[1, :] * np.sign(np.cos(Myb[1, :] - pvb))

        Axa1[2] = 2 * Axa[2, :] * np.sign(np.cos(Mxa[2, :] - pva))
        Axb1[2] = 2 * Axb[2, :] * np.sign(np.cos(Mxb[2, :] - pvb))
        Aya1[2] = 2 * Aya[2, :] * np.sign(np.cos(Mya[2, :] - pha))
        Ayb1[2] = 2 * Ayb[2, :] * np.sign(np.cos(Myb[2, :] - phb))

        Cy, covy = np.polyfit(
            (Aya1[1, :] - Ayb1[1, :]), (Ayb1[0, :] - Aya1[0, :]), 1, cov=True
        )
        Cx, covx = np.polyfit(
            (Axa1[1, :] - Axb1[1, :]), (Axb1[0, :] - Axa1[0, :]), 1, cov=True
        )

        BPM_centre = np.zeros(2)
        BPM_error = np.zeros(2)
        BPM_centre[0] = Axa1[0, nbpm] + Axa1[1, nbpm] * Cx[0] + Axa1[2, nbpm] * Cy[0]
        BPM_centre[1] = Aya1[0, nbpm] + Aya1[1, nbpm] * Cy[0] + Aya1[2, nbpm] * Cx[0]

        BPM_error[0] = np.sqrt(
            Axa1[1, nbpm] ** 2.0 * covx[0, 0] + Axa1[2, nbpm] ** 2.0 * covy[0, 0]
        )
        BPM_error[1] = np.sqrt(
            Aya1[1, nbpm] ** 2.0 * covy[0, 0] + Aya1[2, nbpm] ** 2.0 * covx[0, 0]
        )
        # print 'Estoy Aqui; fin GetOffsets'

        # BPM_centre_uCA = np.zeros(2)
        # BPM_error_uCA = np.zeros(2)
        # BPM_centre_uCA[0] = Axa1[0, nbpm] + Axa1[1, nbpm] * Cx[0]
        # BPM_centre_uCA[1] = Aya1[0, nbpm] + Aya1[1, nbpm] * Cy[0]
        # BPM_error_uCA[0] = np.sqrt(Axa1[1, nbpm] ** 2 * covx[0, nbpm])
        # BPM_error_uCA[1] = np.sqrt(Aya1[1, nbpm] ** 2 * covy[0, nbpm])

        # return [BPM_centre, BPM_error, BPM_centre_uCA, BPM_error_uCA]
        return [BPM_centre, BPM_error]

    def getOffsetsSKEWSDC(self, nbpm, dataXminus, dataXplus, dataYminus, dataYplus):
        """
        Method to obtain offset data for each BPM of Skew DC Measurements.

        :param nbpm: BPM number (1 to 120)
        :param dataXminus: Projection data in horizontal plane with SKEW delta 
        minus.
        :param dataXplus: Projection data in horizontal plane with SKEW delta 
        plus.
        :param dataYminus: Projection data in vertical plane with SKEW delta 
        minus.
        :param dataYplus: Projection data in vertical plane with SKEW delta 
        plus.

        :return: List with all the results obtained:[BPM_centre, BPM_error]

        """

        Axa = dataXminus[0]
        Mxa = dataXminus[1]
        Axb = dataXplus[0]
        Mxb = dataXplus[1]
        Aya = dataYminus[0]
        Mya = dataYminus[1]
        Ayb = dataYplus[0]
        Myb = dataYplus[1]

        Axa1 = np.zeros([3, 120])
        Axb1 = np.zeros([3, 120])
        Aya1 = np.zeros([3, 120])
        Ayb1 = np.zeros([3, 120])

        Axa1[0] = 2 * Axa[0, :] * np.sign(np.cos(Mxa[0, :]))
        Axb1[0] = 2 * Axb[0, :] * np.sign(np.cos(Mxb[0, :]))
        Aya1[0] = 2 * Aya[0, :] * np.sign(np.cos(Mya[0, :]))
        Ayb1[0] = 2 * Ayb[0, :] * np.sign(np.cos(Myb[0, :]))

        pha, phb = self.findphasesDC(Mxa[1, :], Mxb[1, :])
        pva, pvb = self.findphasesDC(Mya[1, :], Myb[1, :])

        Axa1[1] = 2 * Axa[1, :] * np.sign(np.cos(Mxa[1, :] - pha))
        Axb1[1] = 2 * Axb[1, :] * np.sign(np.cos(Mxb[1, :] - phb))
        Aya1[1] = 2 * Aya[1, :] * np.sign(np.cos(Mya[1, :] - pva))
        Ayb1[1] = 2 * Ayb[1, :] * np.sign(np.cos(Myb[1, :] - pvb))

        Axa1[2] = 2 * Axa[2, :] * np.sign(np.cos(Mxa[2, :] - pva))
        Axb1[2] = 2 * Axb[2, :] * np.sign(np.cos(Mxb[2, :] - pvb))
        Aya1[2] = 2 * Aya[2, :] * np.sign(np.cos(Mya[2, :] - pha))
        Ayb1[2] = 2 * Ayb[2, :] * np.sign(np.cos(Myb[2, :] - phb))

        Cy, covy = np.polyfit(
            (Aya1[2, :] - Ayb1[2, :]), (Ayb1[0, :] - Aya1[0, :]), 1, cov=True
        )
        Cx, covx = np.polyfit(
            (Axa1[2, :] - Axb1[2, :]), (Axb1[0, :] - Axa1[0, :]), 1, cov=True
        )

        BPM_centre = np.zeros(2)
        BPM_error = np.zeros(2)
        BPM_centre[0] = Axa1[0, nbpm] + Axa1[1, nbpm] * Cy[0] + Axa1[2, nbpm] * Cx[0]
        BPM_centre[1] = Aya1[0, nbpm] + Aya1[1, nbpm] * Cx[0] + Aya1[2, nbpm] * Cy[0]
        BPM_error[0] = np.sqrt(
            Axa1[1, nbpm] ** 2.0 * covy[0, 0] + Axa1[2, nbpm] ** 2.0 * covx[0, 0]
        )
        BPM_error[1] = np.sqrt(
            Aya1[1, nbpm] ** 2.0 * covx[0, 0] + Aya1[2, nbpm] ** 2.0 * covy[0, 0]
        )

        return [BPM_centre, BPM_error]

    def getOffsetsSKEWSAC(self, nbpm, dataX, dataY):
        """
        Method to obtain offset data for each BPM of Skew AC Measurements.

        :param nbpm: BPM number (1 to 120)
        :param dataX: Projection data in horizontal plane with SKEW AC.
        :param dataY: Projection data in vertical plane with SKEW AC.

        :return: List with all the results obtained:[BPM_centre, BPM_error]

        """

        Ax0 = dataX[0]
        Mx = dataX[1]
        Ay0 = dataY[0]
        My = dataY[1]

        Ax = np.zeros([6, 120])
        Ay = np.zeros([6, 120])

        Ax[0] = 2 * Ax0[0, :] * np.sign(np.cos(Mx[0, :]))
        Ay[0] = 2 * Ay0[0, :] * np.sign(np.cos(My[0, :]))

        # ph, pv, ps = self.findphasesAC(Mx[1, :], My[1, :])
        ph, pv, ps = self.findphasesAC(Mx, My)

        Ax[1] = 2 * Ax0[1, :] * np.sign(np.cos(Mx[1, :] - ph))
        Ay[1] = 2 * Ay0[1, :] * np.sign(np.cos(My[1, :] - pv))

        Ax[2] = 2 * Ax0[2, :] * np.sign(np.cos(Mx[2, :] - pv))
        Ay[2] = 2 * Ay0[2, :] * np.sign(np.cos(My[2, :] - ph))

        Ax[3] = 2 * Ax0[3, :] * np.sign(np.cos(Mx[3, :] - ps))
        Ay[3] = 2 * Ay0[3, :] * np.sign(np.cos(My[3, :] - ps))

        Ax[4] = 2 * Ax0[4, :] * np.sign(np.cos(Mx[4, :] - pv - ps))
        Ay[4] = 2 * Ay0[4, :] * np.sign(np.cos(My[4, :] - ph - ps))

        Ax[5, :] = 2 * Ax0[5, :] * np.sign(np.cos(Mx[5, :] - pv + ps))
        Ay[5, :] = 2 * Ay0[5, :] * np.sign(np.cos(My[5, :] - ph + ps))

        Cy, covy = np.polyfit((Ay[4, :] + Ay[5, :]), Ay[3, :], 1, cov=True)
        Cx, covx = np.polyfit((Ax[4, :] + Ax[5, :]), Ax[3, :], 1, cov=True)

        BPM_centre = np.zeros(2)
        BPM_error = np.zeros(2)
        BPM_centre[0] = Ax[0, nbpm] - Ax[1, nbpm] * Cy[0] - Ax[2, nbpm] * Cx[0]
        BPM_centre[1] = Ay[0, nbpm] - Ay[1, nbpm] * Cx[0] - Ay[2, nbpm] * Cy[0]
        BPM_error[0] = np.sqrt(
            Ax[1, nbpm] ** 2.0 * covy[0, 0] + Ax[2, nbpm] ** 2.0 * covx[0, 0]
        )
        BPM_error[1] = np.sqrt(
            Ay[1, nbpm] ** 2.0 * covx[0, 0] + Ay[2, nbpm] ** 2.0 * covy[0, 0]
        )

        return [BPM_centre, BPM_error]

    def findphasesDC(self, ma, mb):
        """
        Method to obtain the excitation frequencies phases (phases of the orbit 
        corrector magnets) that are common to all the BPMs. The phases are 
        obtained respect to the starting time of the acquisition.
        Synchronous acquisition of the BPM data is assumed.

        :param ma: Horizontal plane data
        :param mb: Vertical plane data
        :return: Horizontal and vertical phases

        """

        m_sum = np.mod(ma + mb + np.pi, 2 * np.pi) - np.pi
        good = np.abs(m_sum - np.mean(m_sum)) < 2 * np.std(m_sum)
        phi_sum = np.mean(m_sum[good])

        m_dif = np.mod(ma - mb + np.pi, 2 * np.pi) - np.pi
        good = np.abs(m_dif - np.mean(m_dif)) < 2 * np.std(m_dif)
        phi_dif = np.mean(m_dif[good])

        pa = (phi_sum + phi_dif) / 2
        pb = (phi_sum - phi_dif) / 2

        return pa, pb

    def findphasesAC(self, Mx, My):
        """
        Method to obtain the excitation frequencies phases (phases of the 
        orbit corrector magnets and skew magnet) that are common to all the 
        BPMs. The phases are obtained respect to the starting time of the 
        acquisition. Synchronous acquisition of the BPM data is assumed.

        :param Mx: X plane data
        :param My: X plane data
        :return: phases matrices on horizontal, vertical and longitudinal 
        planes.
        """

        m_h = (np.mod(2 * Mx[1, :], 2 * np.pi)) / 2
        good = np.abs(m_h - np.mean(m_h)) < 2 * np.std(m_h)
        phi_h = np.mean(m_h[good])

        m_v = (np.mod(2 * My[1, :], 2 * np.pi)) / 2
        good = np.abs(m_v - np.mean(m_v)) < 2 * np.std(m_v)
        phi_v = np.mean(m_v[good])

        m_s = [1, 2, 3, 4, 5, 6]

        m_s[0] = (np.mod(2 * (My[4, :] - phi_h), 2 * np.pi)) / 2
        m_s[1] = (np.mod(2 * (phi_h - My[5, :]), 2 * np.pi)) / 2
        m_s[2] = (np.mod(2 * (Mx[4, :] - phi_v), 2 * np.pi)) / 2
        m_s[3] = (np.mod(2 * (phi_v - Mx[5, :]), 2 * np.pi)) / 2
        m_s[4] = (np.mod(2 * Mx[3, :], 2 * np.pi)) / 2
        m_s[5] = (np.mod(2 * My[3, :], 2 * np.pi)) / 2
        m_s = np.array(m_s)
        good = np.abs(m_s - np.mean(m_s)) < 2 * np.std(m_s)
        good = np.vstack((good))

        phi_s = np.mean(m_s[good])

        return phi_h, phi_v, phi_s

    def getProjection(self, dataAX, freqs, skew=False):
        """
        Method to obtain the projections of each BPM measured. The Projections 
        are amplitude and phase matrices of BPM measured at a certain 
        excitation frequency. Each matrix size is 
        [N (BPMs) X M (frequency/ies)].

        :param dataAX: Raw data captured with Fast Archiver.
        :param freqs: Freqs to analyze the data.
        :param skew: If skew, we change parameter N.

        :return: list of 2 elements (2 matrices): [amplitude, phase]

        """

        np.seterr(divide="ignore", invalid="ignore")

        n = 120
        if skew == False:
            N = 1.5 * self.freq_ad
        else:
            N = (6 * self.freq_ad) - 1
        n0 = np.size(dataAX, 1)
        nmax = np.size(freqs)

        comp = np.exp(-2 * np.pi * np.array([np.arange(0, N)]).T * freqs * 1j)
        # s = np.dot(dataAX, comp).T
        R = np.dot(dataAX, np.cos(2 * np.pi * np.array([np.arange(0, N)]).T * freqs)).T
        I = np.dot(dataAX, -np.sin(2 * np.pi * np.array([np.arange(0, N)]).T * freqs)).T

        isthezero = 0
        nfreq = np.size(freqs)

        # Amax0 = np.sqrt(R ** 2 + I ** 2) / N
        # mumax0 = np.angle(s)

        f_pu = (
            np.array(freqs) * np.ones((nfreq, nfreq))
            + (np.array(freqs) * np.ones((nfreq, nfreq))).T
        )
        f_mi = (
            np.array(freqs) * np.ones((nfreq, nfreq))
            - (np.array(freqs) * np.ones((nfreq, nfreq))).T
        )
        chi_pu = np.divide(
            (1 - np.exp(-2 * np.pi * 1j * n0 * f_pu)),
            (1 - np.exp(-2 * np.pi * 1j * f_pu)),
        )
        chi_mi = np.divide(
            (1 - np.exp(-2 * np.pi * 1j * n0 * f_mi)),
            (1 - np.exp(-2 * np.pi * 1j * f_mi)),
        )

        for i in chi_pu:
            for j, k in enumerate(i):
                if np.isnan(i[j]):
                    i[j] = n0

        for i in chi_mi:
            for j, k in enumerate(i):
                if np.isnan(i[j]):
                    i[j] = n0

        chi_sum = chi_mi + chi_pu
        chi_dif = chi_mi - chi_pu
        alpha = np.real(chi_sum)
        beta = np.imag(chi_sum)
        gamma = np.real(chi_dif)
        delta = np.imag(chi_dif)

        M = np.vstack([np.hstack([alpha, -delta.T]), np.hstack([beta.T, gamma])]) / 2

        isimagzero = nmax + isthezero

        M = np.delete(M, isimagzero, 0)
        M = np.delete(M, isimagzero, 1)

        Ve = np.vstack([R, I])
        Ve = np.delete(Ve, isimagzero, 0)

        X = np.linalg.solve(M, Ve)

        X_f = np.vstack((X[0:isimagzero, :], np.zeros((1, n))))
        X_final = np.vstack((X_f, X[isimagzero:, :]))

        Amax = (
            np.sqrt(X_final[0:nmax, :] ** 2.0 + X_final[nmax : (2 * nmax), :] ** 2.0)
            / 2.0
        )
        mumax = np.angle(X_final[nmax : (2 * nmax), :] * 1j + X_final[0:nmax, :])

        np.seterr(divide="raise", invalid="raise")

        return [Amax, mumax]

    def prepareFRM(self, corrH, corrV):
        """
        Method to recover the correctors to FRM mode

        :param corrH: HCM DS name to recover.
        :param corrV: VCM DS name to recover.

        :return: Nothing

        """

        self.restoreCorrWF(corrH)
        self.restoreCorrWF(corrV)

    def saveData(self, hour_filename, day_filename):
        """
        Method to save the data once the measurement finish. The method aves 
        the data in 2 formats NPY and MAT. \n
        We save:
        * Projections file: Projection data for each BPM. To load it 
        correctly(Python): np.load('path/to/file/filename.npy').flat[0]
        * Offsets file: For each BPM we save [BPM_Centre, BPM_error, 
        BPM_Centre_uCA, BPM_error_uCA]. To load it correctly: 
        np.load('path/to/file/filename.npy').flat[0]
        Matlab file contains the same data. We save a dict., so when you load 
        the data you will have for each BPM meassured one variable.

        """
        folder_name = datetime.datetime.now().strftime("%Y")
        dir_name = "/data/SR/FBBA/" + folder_name + "/Measurements"

        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        # hour_filename = datetime.datetime.now().strftime("%H%M%S")
        # day_filename = datetime.datetime.now().strftime("%Y%m%d")

        if self.cb_select.currentIndex() == 1:

            # np.save(dir_name + '/Projections_QUAD_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.npy', self.projectionDict)
            np.save(
                dir_name
                + "/Delta_Offsets_QUAD_MEASUREMENT_"
                + day_filename
                + "_"
                + hour_filename
                + "_FBBA.npy",
                self.offsetsDict,
            )

            # sio.savemat(dir_name + '/Projections_QUAD_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.projectionDict)
            # sio.savemat(dir_name + '/Delta_Offsets_QUAD_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.offsetsDict)
            # sio.savemat(dir_name + '/Machine_Offsets_AutoSaved_QUAD_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.actualOffsets)

        if self.cb_select.currentIndex() == 2:
            if self.chb_dc.isChecked():
                # np.save(dir_name + '/Projections_SKEW_DC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.npy', self.projectionDict)
                np.save(
                    dir_name
                    + "/Delta_Offsets_SKEW_DC_MEASUREMENT_"
                    + day_filename
                    + "_"
                    + hour_filename
                    + "_FBBA.npy",
                    self.offsetsDict,
                )
                # np.save(dir_name + '/Machine_Offsets_AutoSaved_SKEW_DC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.npy',self.actualOffsets)

                # sio.savemat(dir_name + '/Projections_SKEW_DC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat',self.projectionDict)
                # sio.savemat(dir_name + '/Delta_Offsets_SKEW_DC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.offsetsDict)
                # sio.savemat(dir_name + '/Machine_Offsets_AutoSaved_SKEW_DC_MEASUREMENT_' + hour_filename + '_FBBA.mat', self.actualOffsets)

            if self.chb_ac.isChecked():
                # np.save(dir_name + '/Projections_SKEW_AC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.npy', self.projectionDict)
                np.save(
                    dir_name
                    + "/Delta_Offsets_SKEW_AC_MEASUREMENT_"
                    + day_filename
                    + "_"
                    + hour_filename
                    + "_FBBA.npy",
                    self.offsetsDict,
                )
                # np.save(dir_name + '/Machine_Offsets_AutoSaved_SKEW_AC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.npy',self.actualOffsets)

                # sio.savemat(dir_name + '/Projections_SKEW_AC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.projectionDict)
                # sio.savemat(dir_name + '/Delta_Offsets_SKEW_AC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.offsetsDict)
                # sio.savemat(dir_name + '/Machine_Offsets_AutoSaved_SKEW_AC_MEASUREMENT_' + day_filename + '_' + hour_filename + '_FBBA.mat', self.actualOffsets)

    def chageRefFile(self):

        """
        Method to change reference file for delta offsets.
        :return: Nothing
        """

        self.logWriter(self.logFile, "Start to change reference file.")

        # Obtain new reference file:
        new_ref = QtGui.QFileDialog.getOpenFileName(
            self, "OpenFile", "/data/SR/FBBA/", "Numpy Files (*.npy)"
        )

        # Obtain date to add to actual reference file name.
        old_datetime = datetime.datetime.now().strftime("%Y%m%d")
        # Rename actual reference file
        os.rename(
            "/data/SR/FBBA/REF/reference_file.npy",
            "/data/SR/FBBA/REF/old_reference_file_" + old_datetime + ".npy",
        )

        # Copy and rename new reference file.
        shutil.copyfile(str(new_ref), "/data/SR/FBBA/REF/reference_file.npy")

        print(str(new_ref) + "--> New Referecne file!")

        reference = np.load(str(new_ref)).flat[0]
        x = np.arange(len(reference))

        # print(reference)

        topRefValues = []
        bottomRefValues = []
        tickLabel = []
        posTickLabel = []

        for pos, i in enumerate(sorted(reference.keys())):

            print(reference[i][0][0])

            topRefValues.append(reference[i][0][0])
            bottomRefValues.append(reference[i][0][1])
            if i[-2:] == "01":
                tickLabel.append(i[:4] + "/" + i[-6:])
                posTickLabel.append(pos)

        self.tb_console.append(str(new_ref) + "--> New Referecne file!")
        self.logWriter(self.logFile, str(new_ref) + "--> New Referecne file!")
        app.processEvents()

        plt.figure(1)

        plt.subplot(211)
        plt.title("BPMs Delta Offset - New Reference File")
        plt.plot(x, topRefValues, color="black", marker="o")
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("X Reference [mm]")
        plt.grid(True)

        plt.subplot(212)
        plt.plot(x, bottomRefValues, color="black", marker="o")
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("Y Reference [mm]")
        plt.grid(True)

        plt.show()

    def plotData(self):
        """
        Plot method to crosscheck the results with Matlab.

        :return: Nothing

        """
        reference = np.load("/data/SR/FBBA/REF/reference_file.npy").flat[0]
        x = np.arange(len(self.offsetsDict))

        topOffsetValues = []
        bottomOffsetValues = []
        topRefValues = []
        bottomRefValues = []

        topErrorValues = []
        bottomErrorValues = []
        topRefErrorValues = []
        bottomRefErrorValues = []
        tickLabel = []
        posTickLabel = []

        for pos, i in enumerate(self.bpmDS):
            name_bpm = i.replace("/", "_")
            name_bpm = name_bpm.replace("-", "_")
            topOffsetValues.append(self.offsetsDict[name_bpm][0][0])
            bottomOffsetValues.append(self.offsetsDict[name_bpm][0][1])
            topRefValues.append(reference[name_bpm][0][0])
            bottomRefValues.append(reference[name_bpm][0][1])
            topErrorValues.append(self.offsetsDict[name_bpm][1][0])
            bottomErrorValues.append(self.offsetsDict[name_bpm][1][1])
            topRefErrorValues.append(reference[name_bpm][1][0])
            bottomRefErrorValues.append(reference[name_bpm][1][1])

            if self.cb_select.currentIndex() == 1:
                if i[-2:] == "01":
                    tickLabel.append(i[:4] + "/" + i[-6:])
                    posTickLabel.append(pos)
            else:
                if i[-2:] == "03":
                    tickLabel.append(i[:4] + "/" + i[-6:])
                    posTickLabel.append(pos)

        aux_top = []
        aux_bottom = []

        for j in range(len(topOffsetValues)):

            a = topOffsetValues[j]
            b = topRefValues[j]
            aux_top.append(a - b)

            c = bottomOffsetValues[j]
            d = bottomRefValues[j]

            aux_bottom.append(c - d)

        plt.figure(1)
        plt.subplot(211)
        plt.title("Measured BPMs Delta Offset - Reference ")
        plt.plot(x, aux_top, color="blue", marker="o")
        plt.axhline(y=0.1, color="red", linestyle="--")
        plt.axhline(y=-0.1, color="red", linestyle="--")
        # plt.plot(x, (topOffsetValues - topRefValues), color='blue', marker='o')
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("X Dif.Offsets [mm]")
        plt.legend(
            ("Measurement - Reference", "Upper limit", "Lower limit"), loc="upper right"
        )
        plt.grid(True)
        plt.subplot(212)
        plt.plot(x, aux_bottom, color="blue", marker="o")
        plt.axhline(y=0.1, color="red", linestyle="--")
        plt.axhline(y=-0.1, color="red", linestyle="--")
        # plt.plot(x, (bottomOffsetValues - bottomRefValues), color='blue', marker='o')
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("Y Dif.Offsets [mm]")
        plt.legend(
            ("Measurement - Reference", "Upper limit", "Lower limit"), loc="upper right"
        )
        plt.grid(True)

        plt.show(block=False)

        plt.figure(2)
        plt.subplot(211)
        plt.title("Measured BPMs Delta Offset")
        plt.errorbar(x, topOffsetValues, yerr=topErrorValues, marker="o")
        plt.errorbar(
            x,
            topRefValues,
            yerr=topRefErrorValues,
            color="r",
            linestyle="--",
            marker="o",
        )
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("X Offsets [mm]")
        plt.legend(("Measurement", "Reference"), loc="upper right")
        plt.grid(True)
        plt.subplot(212)
        plt.errorbar(x, bottomOffsetValues, yerr=bottomErrorValues, marker="o")
        plt.errorbar(
            x,
            bottomRefValues,
            yerr=bottomRefErrorValues,
            color="r",
            linestyle="--",
            marker="o",
        )
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("Y Offsets [mm]")
        plt.legend(("Measurement", "Reference"), loc="upper right")
        plt.grid(True)

        plt.show()

    def PlotDataButton(self):

        """
        Metohd to plot delta offsets from a file.

        :return: Nothing
        """
        reference = np.load("/data/SR/FBBA/REF/reference_file.npy").flat[0]
        # topOffsetValues = []
        # bottomOffsetValues = []

        # topErrorValues = []
        # bottomErrorValues = []

        topOffsetValues = []
        bottomOffsetValues = []
        topRefValues = []
        bottomRefValues = []

        topErrorValues = []
        bottomErrorValues = []
        topRefErrorValues = []
        bottomRefErrorValues = []
        tickLabel = []
        posTickLabel = []

        year_file = datetime.datetime.now().strftime("%Y")

        fileName = QtGui.QFileDialog.getOpenFileName(
            self,
            "OpenFile",
            "/data/SR/FBBA/" + year_file + "/Measurements/",
            "Numpy Files (*.npy)",
        )

        filePlot = np.load(fileName).flat[0]

        checkplot = str(fileName)
        checkplot = checkplot.split("_")

        bpmsToLoad = list(filePlot.keys())

        bpmsToPlot = []

        for k in bpmsToLoad:
            a = k.replace("_", "/")
            b = a.rfind("/")
            a = a[:b] + a[b:].replace("/", "-")

            bpmsToPlot.append(a)

        bpmsToPlot.sort()
        # print(bpmsToPlot)
        # print(reference.keys())

        xlabels = []

        for t in bpmsToLoad:
            a = t.replace("_", "/")
            xlabels.append(a.replace("-", "_"))

        x = np.arange(len(bpmsToLoad))
        tickLabel = []
        posTickLabel = []

        for pos, i in enumerate(sorted(bpmsToLoad)):
            topOffsetValues.append(filePlot[i][0][0])
            bottomOffsetValues.append(filePlot[i][0][1])
            topRefValues.append(reference[i][0][0])
            bottomRefValues.append(reference[i][0][1])
            topErrorValues.append(filePlot[i][1][0])
            bottomErrorValues.append(filePlot[i][1][1])
            topRefErrorValues.append(reference[i][1][0])
            bottomRefErrorValues.append(reference[i][1][1])
            if "QUAD" in checkplot:
                if i[-2:] == "01":
                    tickLabel.append(i[:4] + "/" + i[-6:])
                    posTickLabel.append(pos)
            else:
                if i[-2:] == "03":
                    tickLabel.append(i[:4] + "/" + i[-6:])
                    posTickLabel.append(pos)

        plt.figure()
        plt.subplot(211)
        plt.title("Measured BPMs Delta Offset")
        plt.errorbar(x, topOffsetValues, yerr=topErrorValues, marker="o")
        plt.errorbar(
            x,
            topRefValues,
            yerr=topRefErrorValues,
            color="r",
            linestyle="--",
            marker="o",
        )
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("X Offsets [mm]")
        plt.legend(("Measurement", "Reference"), loc="upper right")
        plt.grid(True)
        plt.subplot(212)
        plt.errorbar(x, bottomOffsetValues, yerr=bottomErrorValues, marker="o")
        plt.errorbar(
            x,
            bottomRefValues,
            yerr=bottomRefErrorValues,
            color="r",
            linestyle="--",
            marker="o",
        )
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("Y Offsets [mm]")
        plt.legend(("Measurement", "Reference"), loc="upper right")
        plt.grid(True)

        plt.show()

    def saveOffsets(self):

        """
        Method to save the current offsets of the machine.

        :return: None
        """
        self.tb_console.append("Saving offsets.")
        self.logWriter(self.logFile, "Saving offsets.")
        app.processEvents()

        folder_name = datetime.datetime.now().strftime("%Y")
        dir_name = "/data/SR/FBBA/UserSaved/" + folder_name

        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        hour_filename = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

        bpmList = acbdConstantsFBBA.quads_measure_dict.keys()

        bpmList.sort()

        devices = []
        for i in bpmList:
            devices.append(acbdConstantsFBBA.quads_measure_dict[i][0])

        dictOldOffsets = {}

        try:
            for j in devices:
                taurus.Device(j).ParamSet()
                time.sleep(0.1)
                offsetX = float(taurus.Attribute(j + "/Xoffset").read().value)
                offsetY = float(taurus.Attribute(j + "/Zoffset").read().value)

                dictOldOffsets[j] = [offsetX, offsetY]
                time.sleep(0.1)
                app.processEvents()

            np.save(
                dir_name + "/Machine_BPM_Offsets_User_Saved" + hour_filename + ".npy",
                dictOldOffsets,
            )

        except:

            self.tb_console.append("Error saving the BPM Offsets.")
            self.logWriter(self.logFile, "Error saving the BPM Offsets.")
            self.tb_console.append(traceback.print_exc())
            self.logWriter(self.logFile, traceback.print_exc())
            app.processEvents()

        self.tb_console.append("Offsets saved.")
        self.logWriter(self.logFile, "Offsets saved.")
        app.processEvents()

    def recoverOffsets(self):
        """
        Method to recover all BPM offsets from a previous saved file.

        :return: None
        """
        self.tb_console.append("Start recover offsets")
        self.logWriter(self.logFile, "Start recover offsets")
        app.processEvents()

        year_file = datetime.datetime.now().strftime("%Y")

        fileName = QtGui.QFileDialog.getOpenFileName(
            self,
            "OpenFile",
            "/data/SR/FBBA/" + year_file + "/Autosaved_offsets/",
            "Numpy Files (*Machine*.npy)",
        )

        try:
            if "Machine" in fileName:
                fileOffsets = np.load(fileName).flat[0]
                devices = fileOffsets.keys()
                devices.sort()

                for i in devices:
                    # print i

                    taurus.Device(i).ParamGet()
                    time.sleep(0.01)
                    taurus.Attribute(i + "/Xoffset").write(fileOffsets[i][0])
                    taurus.Attribute(i + "/Zoffset").write(fileOffsets[i][1])
                    time.sleep(0.1)

                    taurus.Device(i).ParamSet()
                    time.sleep(0.1)
            else:
                raise ValueError("This file is not offsets file.")
        except ValueError:
            self.tb_console.append("Error: the file is not an Offsets File")
            self.logWriter(self.logFile, "Error: the file is not an Offsets File")
            app.processEvents()

        except:
            self.tb_console.append("Error recovering Offsets")
            self.logWriter(self.logFile, "Error recovering Offsets")
            self.tb_console.append(traceback.print_exc())
            self.logWriter(self.logFile, traceback.print_exc())
            app.processEvents()

    def ApplyAndSave(self, bpmacq, bpm):
        """
        Method to save the current offsets in BPMs to be measured. First of 
        all we do a ParamSet() command to be sure of the value saved in the 
        BPM-ACQ as (X/Z)offset is the same in the libera.
        At the end of the measurement we save this.

        :param bpmacq: List of BPM-ACQ device servers involved in the 
        measurement.
        :param bpm: List of BPM involved in the measurement
        :return: Nothing
        """

        actualOffsets = {}

        self.tb_console.append("Saving actual offsets.")
        self.logWriter(self.logFile, "Saving actual offsets.")
        app.processEvents()

        for j, i in enumerate(bpmacq):
            taurus.Device(i).ParamGet()
            time.sleep(0.05)
            taurus.Device(i).ParamSet()
            x = np.float(taurus.Attribute(i + "/Xoffset").read().rvalue.magnitude)
            z = np.float(taurus.Attribute(i + "/Zoffset").read().rvalue.magnitude)
            name_bpm = bpm[j].replace("/", "_")
            name_bpm = name_bpm.replace("-", "_")
            actualOffsets[name_bpm] = [x, z]
            time.sleep(0.05)

        # self.actualOffsets = actualOffsets

        folder_name = datetime.datetime.now().strftime("%Y")
        dir_name = "/data/SR/FBBA/" + folder_name + "/Autosaved_offsets"

        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        hour_filename = datetime.datetime.now().strftime("%H%M%S")
        day_filename = datetime.datetime.now().strftime("%Y%m%d")

        if self.cb_select.currentIndex() == 1:
            np.save(
                dir_name
                + "/Machine_Offsets_AutoSaved_QUAD_MEASUREMENT_"
                + day_filename
                + "_"
                + hour_filename
                + "_FBBA.npy",
                actualOffsets,
            )

        if self.cb_select.currentIndex() == 2:
            if self.chb_dc.isChecked():
                np.save(
                    dir_name
                    + "/Machine_Offsets_AutoSaved_SKEW_DC_MEASUREMENT_"
                    + day_filename
                    + "_"
                    + hour_filename
                    + "_FBBA.npy",
                    actualOffsets,
                )

            if self.chb_ac.isChecked():
                np.save(
                    dir_name
                    + "/Machine_Offsets_AutoSaved_SKEW_AC_MEASUREMENT_"
                    + day_filename
                    + "_"
                    + hour_filename
                    + "_FBBA.npy",
                    actualOffsets,
                )

        self.tb_console.append("End Save Actual Offsets.")
        self.logWriter(self.logFile, "End Save Actual Offsets.")
        self.tb_console.append("Wait next steps...")
        app.processEvents()

    def startMeasure(self):
        """
        Main method. Here we will start the measurement and use the method 
        described previously.

        :return: Nothing
        """
        condition = self.checkSetting()
        self.stop = False
        self.pb_stop.setEnabled(True)
        app.processEvents()
        if condition:
            try:
                if self.cb_select.currentIndex() == 1:
                    self.tb_console.append("Welcome to FBBA!")
                    self.tb_console.append("Start Preparation. Please wait.")
                    self.logWriter(self.logFile, "Start Preparation. Please wait.")
                    app.processEvents()
                    self.SKEWS = []
                    self.bpmDS = self.getBPMListQuads()
                    (
                        self.HCM,
                        self.VCM,
                        self.bpmsACQ,
                        self.QUADS,
                        self.bpmNumber,
                    ) = self.getMagnets(self.bpmDS)
                    app.processEvents()
                    self.prepareFBBA(self.HCM, self.VCM)
                    app.processEvents()
                    self.ApplyAndSave(self.bpmsACQ, self.bpmDS)
                    app.processEvents()

                    maximum = len(self.bpmDS)
                    paso = 100.0 / maximum

                    for i, j in enumerate(self.bpmDS):
                        if not self.stop:
                            v = paso * (float(i) + 1)
                            self.progressBar.setValue(v)
                            app.processEvents()
                            self.measurebpm(
                                j,
                                self.HCM[i],
                                self.VCM[i],
                                self.QUADS[i],
                                self.bpmNumber[i],
                            )
                            app.processEvents()
                        else:
                            for k in range(len(self.HCM)):
                                self.corrWVFStop(self.HCM[k], self.VCM[k])
                                app.processEvents()
                            self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                            app.processEvents()
                            break
                    v = paso * (float(i) + 1)
                    self.progressBar.setValue(v)

                    self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                    self.plotData()
                    self.pb_stop.setEnabled(False)
                    app.processEvents()

                    print("Quadrupole Measurement Done and Saved. Have a nice shift!")
                    self.tb_console.append(
                        "Quadrupole Measurement Done and Saved. Have a nice shift!"
                    )
                    self.logWriter(
                        self.logFile,
                        "Quadrupole Measurement Done and Saved. Have a nice shift!",
                    )
                    app.processEvents()

                elif self.cb_select.currentIndex() == 2:
                    self.QUADS = []
                    self.bpmDS = self.getBPMListSkews()
                    (
                        self.HCM,
                        self.VCM,
                        self.bpmsACQ,
                        self.SKEWS,
                        self.bpmNumber,
                    ) = self.getMagnets(self.bpmDS)

                    if self.chb_dc.isChecked():

                        self.prepareFBBA(self.HCM, self.VCM)
                        app.processEvents()
                        self.ApplyAndSave(self.bpmsACQ, self.bpmDS)
                        app.processEvents()

                        maximum = len(self.bpmDS)
                        paso = 100 / maximum

                        for i, j in enumerate(self.bpmDS):
                            if not self.stop:
                                self.progressBar.setValue(paso * (i + 1))
                                self.measurebpmSkewDC(
                                    j,
                                    self.HCM[i],
                                    self.VCM[i],
                                    self.SKEWS[i],
                                    self.bpmNumber[i],
                                )
                                app.processEvents()
                            else:
                                for k in range(len(self.HCM)):
                                    self.corrWVFStop(self.HCM[k], self.VCM[k])
                                    time.sleep(0.01)
                                self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                                app.processEvents()
                                break

                        self.progressBar.setValue(paso * (i + 1))

                        self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                        self.saveData()
                        self.plotData()
                        self.pb_stop.setEnabled(False)
                        app.processEvents()

                        self.tb_console.append(
                            "SKEW DC Measurement Done and Saved. Have a nice shift!"
                        )
                        app.processEvents()

                    if self.chb_ac.isChecked():
                        app.processEvents()
                        self.prepareFBBASkewAC(self.HCM, self.VCM, self.SKEWS)
                        app.processEvents()
                        self.ApplyAndSave(self.bpmsACQ, self.bpmDS)
                        app.processEvents()

                        maximum = len(self.bpmDS)
                        paso = 100 / maximum

                        for i, j in enumerate(self.bpmDS):
                            if not self.stop:
                                self.progressBar.setValue(paso * (i + 1))
                                self.measurebpmSkewAC(
                                    j,
                                    self.HCM[i],
                                    self.VCM[i],
                                    self.SKEWS[i],
                                    self.bpmNumber[i],
                                )
                                app.processEvents()
                            else:
                                for k in range(len(self.HCM)):
                                    self.corrWVFStop(
                                        self.HCM[k], self.VCM[k], self.SKEWS[k]
                                    )
                                    time.sleep(0.01)
                                self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                                app.processEvents()
                                break

                        self.progressBar.setValue(paso * (i + 1))

                        self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                        self.saveData()
                        self.plotData()
                        self.pb_stop.setEnabled(False)
                        app.processEvents()

                        self.tb_console.append(
                            "SKEW AC Measurement Done and Saved. Have a nice shift!"
                        )
                        app.processEvents()

            except:
                self.tb_console.append("Recovering CORR to FRM mode.")
                self.logWriter(self.logFile, "Recovering CORR to FRM mode.")
                self.tb_console.append(traceback.print_exc())
                self.logWriter(self.logFile, traceback.print_exc())
                app.processEvents()
                self.pb_stop.setEnabled(False)
                app.processEvents()
                for i in range(len(self.HCM)):
                    if not self.SKEWS:
                        self.corrWVFStop(self.HCM[i], self.VCM[i])
                        time.sleep(0.01)
                        app.processEvents()
                    else:
                        self.corrWVFStop(self.HCM[i], self.VCM[i], self.SKEWS[i])
                        time.sleep(0.01)
                        app.processEvents()

                self.prepareFRM(self.bkupHCMWF, self.bkupVCMWF)
                self.tb_console.append("Recover done")
                self.logWriter(self.logFile, "Recover done.")
                app.processEvents()

        else:
            self.tb_console.append("Error. Please check measurement.")
            self.logWriter(self.logFile, "Error. Please check measurement.")
            app.processEvents()


class bpmClass(QtGui.QDialog, form_bpm):
    def __init__(self, bpmsSelected=None, parent=None):

        QtGui.QDialog.__init__(self, parent)

        self.setupUi(self)

        self.previousSelected = bpmsSelected
        self.selected = []
        self.chb_list = [
            self.chb_101,
            self.chb_102,
            self.chb_103,
            self.chb_104,
            self.chb_105,
            self.chb_106,
            self.chb_107,
            self.chb_108,
            self.chb_109,
            self.chb_110,
            self.chb_111,
            self.chb_112,
            self.chb_113,
            self.chb_114,
            self.chb_115,
            self.chb_116,
        ]
        self.pb_all.clicked.connect(self.selectAll)
        self.pb_clear.clicked.connect(self.clearAll)
        self.submit.clicked.connect(self.submitclose)
        self.CheckSelected()

    def CheckSelected(self):

        if self.previousSelected:
            for i in self.previousSelected:
                for j in self.chb_list:
                    a = str(j.text())
                    if a.lower() == i:
                        j.setChecked(True)

    def selectAll(self):
        """
        Method to check all the SR check boxes with one click.

        """
        for i in self.chb_list:
            i.setChecked(True)

    def clearAll(self):
        """
        Method to uncheck all the SR check boxes with one click

        """
        for j in self.chb_list:
            j.setChecked(False)

    def submitclose(self):
        """
        Method write the SR sectors to analyze.

        """
        for i in self.chb_list:
            if i.isChecked():
                a = str(i.text())
                self.selected.append(a.lower())
        self.accept()


class errorDialog(QtGui.QMessageBox):
    def __init__(self, type, parent=None):
        QtGui.QMessageBox.__init__(self, parent)

        if type == 1:
            self.setIcon(QtGui.QMessageBox.Critical)
            self.setText("Settings Missed")
            self.setInformativeText(
                "No BPM selected. Please select the BPMs and push Start Again."
            )
            self.setWindowTitle("Error Detected")
            self.setStandardButtons(QtGui.QMessageBox.Ok)
        if type == 2:
            self.setIcon(QtGui.QMessageBox.Critical)
            self.setText("Settings Missed")
            self.setInformativeText(
                "Check that DC mode is checked. With QUADS we only can measure in DC mode."
            )
            self.setWindowTitle("Error Detected")
            self.setStandardButtons(QtGui.QMessageBox.Ok)
        if type == 3:
            self.setIcon(QtGui.QMessageBox.Critical)
            self.setText("Settings Missed")
            self.setInformativeText("Select AC or DC configuration for the SKEWS.")
            self.setWindowTitle("Error Detected")
            self.setStandardButtons(QtGui.QMessageBox.Ok)


class bpmOffsets(QtGui.QWidget, form_class_offsets):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.setupUi(self)
        self.setWindowTitle("Apply FBBA Offsets")

        self.radbuttons = [
            self.rd_011,
            self.rd_012,
            self.rd_013,
            self.rd_014,
            self.rd_015,
            self.rd_016,
            self.rd_017,
            self.rd_021,
            self.rd_022,
            self.rd_023,
            self.rd_024,
            self.rd_025,
            self.rd_026,
            self.rd_027,
            self.rd_028,
            self.rd_031,
            self.rd_032,
            self.rd_033,
            self.rd_034,
            self.rd_035,
            self.rd_036,
            self.rd_037,
            self.rd_038,
            self.rd_041,
            self.rd_042,
            self.rd_043,
            self.rd_044,
            self.rd_045,
            self.rd_046,
            self.rd_047,
            self.rd_051,
            self.rd_052,
            self.rd_053,
            self.rd_054,
            self.rd_055,
            self.rd_056,
            self.rd_057,
            self.rd_061,
            self.rd_062,
            self.rd_063,
            self.rd_064,
            self.rd_065,
            self.rd_066,
            self.rd_067,
            self.rd_068,
            self.rd_071,
            self.rd_072,
            self.rd_073,
            self.rd_074,
            self.rd_075,
            self.rd_076,
            self.rd_077,
            self.rd_078,
            self.rd_081,
            self.rd_082,
            self.rd_083,
            self.rd_084,
            self.rd_085,
            self.rd_086,
            self.rd_087,
            self.rd_091,
            self.rd_092,
            self.rd_093,
            self.rd_094,
            self.rd_095,
            self.rd_096,
            self.rd_097,
            self.rd_101,
            self.rd_102,
            self.rd_103,
            self.rd_104,
            self.rd_105,
            self.rd_106,
            self.rd_107,
            self.rd_108,
            self.rd_111,
            self.rd_112,
            self.rd_113,
            self.rd_114,
            self.rd_115,
            self.rd_116,
            self.rd_117,
            self.rd_118,
            self.rd_121,
            self.rd_122,
            self.rd_123,
            self.rd_124,
            self.rd_125,
            self.rd_126,
            self.rd_127,
            self.rd_131,
            self.rd_132,
            self.rd_133,
            self.rd_134,
            self.rd_135,
            self.rd_136,
            self.rd_137,
            self.rd_141,
            self.rd_142,
            self.rd_143,
            self.rd_144,
            self.rd_145,
            self.rd_146,
            self.rd_147,
            self.rd_148,
            self.rd_151,
            self.rd_152,
            self.rd_153,
            self.rd_154,
            self.rd_155,
            self.rd_156,
            self.rd_157,
            self.rd_158,
            self.rd_161,
            self.rd_162,
            self.rd_163,
            self.rd_164,
            self.rd_165,
            self.rd_166,
            self.rd_167,
        ]

        self.pb_selectAll.clicked.connect(self.selectAll)
        self.pb_unselctAll.clicked.connect(self.unselctAll)

        self.pb_doit.clicked.connect(self.ApplyOffsets)
        self.pb_resp2ref.clicked.connect(self.ApplyOffsetsRespect2Ref)
        self.pb_cancel.clicked.connect(self.cancelamelo)

        self.bpmList = []
        self.bpmsToLoad = []
        self.fileName = None
        self.file = None
        self.logFile = None

        self.initGUI()

    def initGUI(self):

        """
        Starting method. First of all we load a file to work with. Once the 
        file is loaded, we plot datafile and GUI is showed.

        :return: Nothing
        """
        self.logFolderCreator()
        year = datetime.datetime.now().strftime("%Y")
        self.fileName = QtGui.QFileDialog.getOpenFileName(
            self,
            "OpenFile",
            "/data/SR/FBBA/" + year + "/Measurements",
            "Numpy Files (*.npy)",
        )

        self.file = np.load(self.fileName).flat[0]

        bpmsToLoad = list(self.file.keys())

        for k in bpmsToLoad:
            a = k.replace("_", "/")
            b = a.rfind("/")
            a = a[:b] + a[b:].replace("/", "-")

            self.bpmsToLoad.append(a)

        self.bpmsToLoad.sort()

        self.selectBpm(self.bpmsToLoad)

        self.plotData()

    def logFolderCreator(self):
        """
        Method to check and create log folder and file in today folder.
        :return:
        """

        # Checlk if today folder exists
        today = datetime.datetime.now().strftime("%Y%m%d")
        todayFolder = "/data/SR/" + today
        todayFolderBool = os.path.exists(todayFolder)

        # If not, we will create today folder.
        if todayFolderBool == False:
            os.mkdir(todayFolder)

        # Check if FFBALog folder exists and if ot we will create it.
        logFolder = todayFolder + "/FBBALog"
        logFolfderBool = os.path.exists(logFolder)

        if logFolfderBool == False:
            os.mkdir(logFolder)

        self.logFile = logFolder + "/log.txt"
        dateLog = datetime.datetime.now().strftime("%Y%m%d %H:%m:%S")
        with open(self.logFile, "a") as f:
            f.write(dateLog + "\t" + "Start Offsets GUI. \n")
        f.close()

    def logWriter(self, folder, text):
        """
        Method to write a log file.
        :param folder: path to log file.
        :param text: Text to write.
        :return: Nothing.
        """

        dateText = datetime.datetime.now().strftime("%Y%m%d %H:%m:%S")
        toWrite = dateText + "  " + text + "\n"
        fname = folder  # + '/log.txt'

        with open(fname, "a") as f:
            f.write(toWrite)
        f.close()

    def send_mail(self, offsets):
        """
        Method to send e-mail messages once script finish.
        In case of error you can attach a log file to send.

        :param offsets: Log file to send
        :return: Nothing
        """
        ds = sorted(offsets.keys())
        self.logWriter(self.logFile, "Mail sent with changes sent!")
        # Define the smptep server
        server = "smtp.cells.es"
        # Define the sender e-mial
        send_from = "root@crremote01.cells.es"
        # Define the receivers. always a list.
        send_to = [
            "emorales@cells.es",
            "uiriso@cells.es",
            "dyepez@cells.es",
            "zeus@cells.es",
            "gbenedetti@cells.es",
            "ltorino@cells.es",
        ]
        # Message subject
        subject = "Beam Dynamics - FBBA: Change in BPM Offsets"

        # Start to write message:
        msg = MIMEMultipart()
        msg["From"] = "Beam Dynamics - <" + send_from + ">"
        msg["Bcc"] = COMMASPACE.join(send_to)
        msg["Date"] = formatdate(localtime=True)
        msg["Subject"] = subject

        text = "***Mail generated automatically***\n\n\n"
        text += "This mail is to inform you that some BPM Offsets has changed:\n\n"
        for i in ds:
            text += str(i) + " Old X Offset: " + str(offsets[i][0][0]) + "\n"
            text += str(i) + " New X Offset: " + str(offsets[i][1][0]) + "\n\n"

            text += str(i) + " Old Y Offset: " + str(offsets[i][0][1]) + "\n"
            text += str(i) + " New Y Offset: " + str(offsets[i][1][1]) + "\n\n"

        text += "This new offset was calculated with FBBA Python GUI.\n"
        text += "In case of doubt or problems, please contact Beam Dynamics.\n\n\n"
        text += "Kind Regards.\n"
        text += "Diagnostics Section - Beam Dynamics. \n\n\n"
        # Attach message:
        msg.attach(MIMEText(text))

        smtp = smtplib.SMTP(server)
        smtp.sendmail(send_from, send_to, msg.as_string())
        smtp.close()

        # # Attach log file start here:
        # with open(files, "rb") as f:
        #     part = MIMEApplication(
        #         f.read(),
        #         Name=basename(files)
        #     )
        # # After the file is closed
        # part['Content-Disposition'] = 'attachment; filename="%s"' % basename(files)
        # # Attach file
        # msg.attach(part)

    def selectAll(self):
        """
        Metohd to select all checkboxes.

        :return: Nothing
        """

        for i in self.radbuttons:
            if i.isEnabled():
                i.setChecked(True)

    def unselctAll(self):
        """
        Method to un select all the BPM check boxes.

        :return: Nothing
        """

        for i in self.radbuttons:
            if i.isEnabled():
                i.setChecked(False)

    def selectBpm(self, bpmToCheck):
        """
        Method to select the BPMs contained in loaded file.

        :param bpmToCheck: BPMs to activate.

        :return: Nothing.
        """

        for i in self.radbuttons:
            comp = str(i.text())
            for j in bpmToCheck:

                if comp.lower() == j:
                    i.setChecked(True)
                    i.setEnabled(True)

        for k in self.radbuttons:
            if not k.isChecked():
                k.setEnabled(False)

    def plotData(self):
        """
        Method to plot data loaded.
        :return:
        """

        self.logWriter(self.logFile, "Plot data - BPM Dialog.")
        self.bpmList = []

        reference = np.load("/data/SR/FBBA/REF/reference_file.npy").flat[0]

        for i in self.radbuttons:
            if i.isChecked():
                name = str(i.text())
                self.bpmList.append(name.lower())

        x = np.arange(len(self.file))
        tickLabel = []
        posTickLabel = []

        forfile = []
        for j in self.bpmList:
            aux = j.replace("/", "_")
            forfile.append(aux.replace("-", "_"))

        self.topOffsetValues = {}
        self.bottomOffsetValues = {}

        for item in self.file.keys():
            ind_bpm= item.replace('_', '/')
            ind_bpm = ind_bpm[:-3] + '-' + ind_bpm[-2:]
            self.topOffsetValues[ind_bpm] = self.file[item][0][0]
            self.bottomOffsetValues[ind_bpm] = self.file[item][0][1]

        topOffsetValues = []
        bottomOffsetValues = []
        topRefValues = []
        bottomRefValues = []
        topErrorValues = []
        bottomErrorValues = []
        topRefErrorValues = []
        bottomRefErrorValues = []

        checkplot = str(self.file)
        checkplot = checkplot.split("_")

        for pos, i in enumerate(sorted(forfile)):
            topOffsetValues.append(self.file[i][0][0])
            bottomOffsetValues.append(self.file[i][0][1])
            topErrorValues.append(self.file[i][1][0])
            bottomErrorValues.append(self.file[i][1][1])
            topRefValues.append(reference[i][0][0])
            bottomRefValues.append(reference[i][0][1])
            topRefErrorValues.append(reference[i][1][0])
            bottomRefErrorValues.append(reference[i][1][1])
            if "QUAD" in checkplot:
                if i[-2:] == "01":
                    tickLabel.append(i[:4] + "/" + i[-6:])
                    posTickLabel.append(pos)
            else:
                if i[-2:] == "03":
                    tickLabel.append(i[:4] + "/" + i[-6:])
                    posTickLabel.append(pos)

        plt.figure()
        plt.subplot(211)
        plt.title("Measured BPMs Delta Offset")
        plt.errorbar(x, topOffsetValues, yerr=topErrorValues, marker="o")
        plt.errorbar(
            x,
            topRefValues,
            yerr=topRefErrorValues,
            color="r",
            linestyle="--",
            marker="o",
        )
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("X Offsets [mm]")
        plt.legend(("Measurement", "Reference"), loc="upper right")
        plt.grid(True)
        plt.subplot(212)
        plt.errorbar(x, bottomOffsetValues, yerr=bottomErrorValues, marker="o")
        plt.errorbar(
            x,
            bottomRefValues,
            yerr=bottomRefErrorValues,
            color="r",
            linestyle="--",
            marker="o",
        )
        plt.xticks(posTickLabel, tickLabel, rotation=30)
        plt.ylabel("Y Offsets [mm]")
        plt.legend(("Measurement", "Reference"), loc="upper right")
        plt.grid(True)

        plt.show()

    def saveOffsets(self):
        """
        Method to save all BPM offsets. We always use this method every time 
        we want to change BPM/s offset/s.

        :return: Nothing
        """
        self.logWriter(self.logFile, "Save machine offsets.")

        folder_name = datetime.datetime.now().strftime("%Y%m%d")
        dir_name = "/data/SR/FBBA/AutoSavedOffsets/" + folder_name

        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        hour_filename = datetime.datetime.now().strftime("%H%M%S")

        devices = []
        for i in self.bpmList:
            devices.append(acbdConstantsFBBA.quads_measure_dict[i][0])

        dictOldOffsets = {}
        try:
            for j in devices:

                offsetX = float(
                    taurus.Attribute(j + "/Xoffset").read().rvalue.magnitude
                )
                offsetY = float(
                    taurus.Attribute(j + "/Zoffset").read().rvalue.magnitude
                )

                dictOldOffsets[j] = [offsetX, offsetY]
                time.sleep(0.1)

            np.save(
                dir_name + "Machine_BPM_Offsets_Saved" + hour_filename + ".npy",
                dictOldOffsets,
            )
            sio.savemat(
                dir_name + "Machine_BPM_Offsets_Saved" + hour_filename + ".mat",
                dictOldOffsets,
            )

        except Exception:
            print("Error saving the BPM Offsets.")
            traceback.print_exc()

    def ApplyOffsets(self, offset):
        """
        Method to apply offsets in one or more BPMs. The method will send an 
        e-mail every time one BPM offset change with this script.

        :param offset: List of offsets to apply.
        :return: Nothing
        """
        self.logWriter(self.logFile, "Apply offsets")
        auxVector = []
        aux2 = []

        # print offset

        for t, i in enumerate(self.radbuttons):
            if i.isChecked():
                auxVar = str(i.text())
                auxVector.append(auxVar.lower())
                aux2.append(t)

        # print aux2

        name = auxVector

        self.saveOffsets()

        try:
            if len(name) > 1:

                # print 'Appy Offsets (More than 1 BPM)'

                mail = {}
                for position, devname in enumerate(name):

                    acqName = acbdConstantsFBBA.quads_measure_dict[devname][0]

                    oldX = float(taurus.Attribute(acqName + "/Xoffset").read().value)
                    taurus.Attribute(acqName + "/Xoffset").write(
                        oldX + self.topOffsetValues[devname]
                    )
                    time.sleep(0.1)
                    # taurus.Device(acqName).ParamSet()

                    oldZ = float(taurus.Attribute(acqName + "/Zoffset").read().value)
                    taurus.Attribute(acqName + "/Zoffset").write(
                        oldZ + self.bottomOffsetValues[devname]
                    )
                    time.sleep(0.1)
                    mail[devname] = [
                        [oldX, oldZ],
                        [
                            oldX + self.topOffsetValues[devname],
                            oldZ + self.bottomOffsetValues[devname],
                        ],
                    ]
                    taurus.Device(acqName).ParamSet()
                    time.sleep(0.1)
                self.send_mail(mail)
            else:
                if len(name) == 1:

                    acqName = acbdConstantsFBBA.quads_measure_dict[name[0]][0]
                    mail = {}

                    # print 'Appy Offsets (1 BPM)'
                    oldX = float(taurus.Attribute(acqName + "/Xoffset").read().value)
                    taurus.Attribute(acqName + "/Xoffset").write(
                        oldX + self.topOffsetValues[name[0]]
                    )
                    time.sleep(0.1)
                    # taurus.Device(acqName).ParamSet()
                    oldZ = float(taurus.Attribute(acqName + "/Zoffset").read().value)
                    taurus.Attribute(acqName + "/Zoffset").write(
                        oldZ + self.bottomOffsetValues[name[0]]
                    )
                    time.sleep(0.1)
                    taurus.Device(acqName).ParamSet()

                    mail[name[0]] = [
                        [oldX, oldZ],
                        [
                            oldX + self.topOffsetValues[name[0]],
                            oldZ + self.bottomOffsetValues[name[0]],
                        ],
                    ]
                    self.send_mail(mail)
                else:
                    print("Error: No BPM device")
                    self.logWriter(self.logFile, "Apply offsets error: No BPM device")

        except Exception:
            print("Apply offsets error: during the operation")
            traceback.print_exc()
            self.logWriter(self.logFile, "Apply offsets error: during the operation")
            self.logWriter(self.logFile, str(traceback.print_exc()))

    def ApplyOffsetsRespect2Ref(self, offset):
        """
        Method to apply offsets in one or more BPMs respect to referece. The 
        method will send an e-mail every time one BPM offset changewith this 
        script.

        :param offset: List of offsets to apply.
        :return: Nothing
        """

        auxVector = []
        aux2 = []

        reference = np.load("/data/SR/FBBA/REF/reference_file.npy").flat[0]
        self.logWriter(self.logFile, "Apply offsets respect 2 reference start.")

        for t, i in enumerate(self.radbuttons):
            if i.isChecked():
                auxVar = str(i.text())
                auxVector.append(auxVar.lower())
                aux2.append(t)

        print(aux2)
        print(auxVector)
        name = auxVector

        self.saveOffsets()

        try:
            if len(name) > 1:

                print("Change offsets start.")
                mail = {}
                for position, devname in enumerate(name):

                    acqName = acbdConstantsFBBA.quads_measure_dict[devname][0]

                    a = devname.replace("/", "_")
                    foo = a.replace("-", "_")

                    oldX = float(taurus.Attribute(acqName + "/Xoffset").read().value)
                    taurus.Attribute(acqName + "/Xoffset").write(
                        oldX
                        - (reference[foo][0][0] - self.topOffsetValues[devname])
                    )
                    time.sleep(0.1)
                    # taurus.Device(acqName).ParamSet()

                    oldZ = float(taurus.Attribute(acqName + "/Zoffset").read().value)
                    taurus.Attribute(acqName + "/Zoffset").write(
                        oldZ
                        - (
                            reference[foo][0][1]
                            - self.bottomOffsetValues[devname]
                        )
                    )
                    time.sleep(0.1)
                    mail[devname] = [
                        [oldX, oldZ],
                        [
                            oldX
                            - (
                                reference[foo][0][0]
                                - self.topOffsetValues[devname]
                            ),
                            oldZ
                            - (
                                reference[foo][0][1]
                                - self.bottomOffsetValues[devname]
                            ),
                        ],
                    ]
                    taurus.Device(acqName).ParamSet()
                    time.sleep(0.1)

                print("Offsets changed. Mail will be sent to Beam Dynamics group.")
                self.send_mail(mail)
            else:
                if len(name) == 1:

                    print("Change offsets start.")
                    acqName = acbdConstantsFBBA.quads_measure_dict[name[0]][0]

                    a = name[0].replace("/", "_")
                    foo = a.replace("-", "_")

                    mail = {}

                    oldX = float(taurus.Attribute(acqName + "/Xoffset").read().value)
                    taurus.Attribute(acqName + "/Xoffset").write(
                        oldX - (reference[foo][0][0] - self.topOffsetValues[name[0]])
                    )
                    time.sleep(0.1)
                    taurus.Device(acqName).ParamSet()
                    oldZ = float(taurus.Attribute(acqName + "/Zoffset").read().value)
                    taurus.Attribute(acqName + "/Zoffset").write(
                        oldZ - (reference[foo][0][1] - self.bottomOffsetValues[name[0]])
                    )
                    time.sleep(0.1)
                    taurus.Device(acqName).ParamSet()

                    mail[name[0]] = [
                        [oldX, oldZ],
                        [
                            oldX
                            - (reference[foo][0][0] - self.topOffsetValues[name[0]]),
                            oldZ
                            - (reference[foo][0][1] - self.bottomOffsetValues[name[0]]),
                        ],
                    ]

                    print("Offset changed. Mail will be sent to Beam Dynamics group.")
                    self.send_mail(mail)
                else:
                    print("Error: No BPM device")
                    self.logWriter(
                        self.logFile,
                        "Apply offsets respect 2 reference error: No BPM device.",
                    )

        except Exception:
            print("Error during the operation")
            traceback.print_exc()
            self.logWriter(
                self.logFile,
                "Apply offsets respect 2 reference error during operation.",
            )
            self.logWriter(self.logFile, str(traceback.print_exc()))

    def cancelamelo(self):
        """
        Method to close UI form.
        :return:
        """
        self.close()


if __name__ == "__main__":

    app = QtGui.QApplication(sys.argv)
    MyWindow = fbbaMainClass(None)
    MyWindow.show()
    app.exec_()
